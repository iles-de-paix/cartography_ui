import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { Button, Grid } from '@mui/material'
import { ChosenTheme } from 'providers'
import { Icon } from 'components/Icon/Icon'

import './NotFound.scss'

const NotFound: React.FC = () => {
  const { theme } = useContext(ChosenTheme)
  return (
    <Grid container>
      <Grid item xs={12}>
        <div className="notFoundPage">
          {/* // TODO: remove className */}
          <Icon name="not_found" className={theme === 'dark' ? 'invertImg' : ''} alt="Page non trouvée" width={500} />
          <Link to="/">
            <Button variant="contained">Retour sur la page d&apos;accueil</Button>
          </Link>
        </div>
      </Grid>
    </Grid>
  )
}

export default NotFound
