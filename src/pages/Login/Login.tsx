import React, { useState, useContext, useEffect, useCallback } from 'react'

import _find from 'lodash/find'
import useUsers from 'hooks/useUsers'
import { useNavigate } from 'react-router-dom'
import { UserContext } from 'providers/UserProvider'
import { ERole, type Role, type User } from 'models/user'
import { useForm, type SubmitHandler } from 'react-hook-form'

import Card from 'components/Card'
import Snackbar from 'components/Snackbar'
import Loader from 'components/Loader/Loader'
import { Button, CardActions, CardContent, Grid, TextField } from '@mui/material'

import './Login.scss'

interface Inputs {
  firstName: string
  lastName: string
  email: string
}

export interface LoginProps {
  role: Role
}

const Login: React.FC<LoginProps> = ({ role }) => {
  const { users, addUser } = useUsers()
  const navigate = useNavigate()

  const { user, setUser } = useContext(UserContext)

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<Inputs>()
  const [openSnackBar, setOpenSnackBack] = useState(false)
  const [isRegistrationForm, setIsRegistrationForm] = useState(false)

  const handleNavigate = useCallback(
    (_user: User) => {
      switch (role) {
        case 'volunteer':
          return navigate('/volunteer', { replace: true })
        case 'manager':
        case 'admin':
          switch (ERole[_user.role]) {
            case 'volunteer':
              return navigate(-1)
            case 'manager':
              return navigate('/manager', { replace: true })
            case 'admin':
              return navigate('/admin', { replace: true })
            default:
              return navigate(-1)
          }
        default:
          return navigate(-1)
      }
    },
    [navigate, role]
  )

  useEffect(() => {
    if (user && user.email) handleNavigate(user)
  }, [handleNavigate, user])

  const onSubmit: SubmitHandler<Inputs> = _data => {
    if (isRegistrationForm) {
      addUser.mutate({
        email: _data.email,
        firstName: _data.firstName,
        lastName: _data.lastName,
        role: Number(ERole['volunteer'])
      })
    } else {
      if (users?.map((u: User) => u.email).includes(_data.email)) {
        const currentUser: User | undefined = _find(users, ['email', _data.email])
        if (currentUser) {
          setUser(currentUser)
          handleNavigate(currentUser)
        } else {
          setOpenSnackBack(true)
        }
      }
    }
  }

  if (user?.email) return <Loader />

  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Card>
            <CardContent className="loginForm">
              {isRegistrationForm && (
                <>
                  <TextField id="outlined-textarea" label="Prénom" placeholder="Prénom" {...register('firstName')} />
                  <TextField id="outlined-textarea" label="Nom" placeholder="Nom" {...register('lastName')} />
                </>
              )}
              <TextField
                id="outlined-textarea"
                label="Email"
                placeholder="Email"
                helperText={!!errors.email && "L'email est obligatoire"}
                error={!!errors.email}
                required
                type="email"
                autoComplete={'on'}
                {...register('email')}
              />
            </CardContent>
            {isRegistrationForm && role === 'volunteer' ? (
              <CardActions className="loginButtons">
                <Button type="submit" variant="contained">
                  S&apos;inscrire
                </Button>
              </CardActions>
            ) : (
              <CardActions className="loginButtons">
                <Button type="submit" variant="contained">
                  Se connecter
                </Button>
                {role === 'volunteer' && (
                  <Button type="button" onClick={() => setIsRegistrationForm(true)}>
                    S&apos;inscrire
                  </Button>
                )}
              </CardActions>
            )}
          </Card>
        </form>
      </Grid>
      <Snackbar isOpen={openSnackBar} />
    </Grid>
  )
}

export default Login
