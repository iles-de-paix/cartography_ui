import React, { Suspense } from 'react'
import { Grid, Typography } from '@mui/material'
import useCampaignDates from 'hooks/useCampaignDates'

import TabList from 'components/TabList'
import Loader from 'components/Loader/Loader'

import type { Tab } from 'components/TabList/TabList'

const VolunteerDates = React.lazy(() => import('features/VolunteerDates'))
const VolunteerSales = React.lazy(() => import('features/VolunteerSales'))
const History = React.lazy(() => import('features/History'))

const tabs: Tab[] = [
  { name: 'Dates', children: <VolunteerDates /> },
  { name: 'Ventes', children: <VolunteerSales /> },
  { name: 'Historique', children: <History /> }
]

const Volunteer: React.FC = () => {
  const { status } = useCampaignDates()

  return (
    <Grid container spacing={2} className="appWrapper">
      <Grid item xs={12}>
        <Typography variant="h3" component="div" className="centerTitle">
          Bénévole(s)
        </Typography>
        {status === 'error' && <p>Error fetching data</p>}
        {status === 'loading' && <Loader />}
        {status === 'success' && (
          <Suspense fallback={<Loader />}>
            <TabList tabs={tabs} />
          </Suspense>
        )}
      </Grid>
    </Grid>
  )
}

export default Volunteer
