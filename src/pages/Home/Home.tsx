import React, { useContext, useEffect, useState } from 'react'
import { CardActions, CardContent, Grid, Typography } from '@mui/material'
import Card from 'components/Card'
import { Link } from 'react-router-dom'
import { UserContext } from 'providers/UserProvider'
import { Icon, type IconName } from 'components/Icon/Icon'

import './Home.scss'

const images: IconName[] = ['volunteer_1', 'volunteer_2', 'volunteer_3']

const Home: React.FC = () => {
  const { user } = useContext(UserContext)
  const [volunteerImgIndex, setVolunteerImgIndex] = useState(0)

  useEffect(() => {
    const interval = setInterval(() => {
      if (volunteerImgIndex === 2) setVolunteerImgIndex(0)
      else setVolunteerImgIndex(prevVolunteerImgIndex => prevVolunteerImgIndex + 1)
    }, 10000)
    return () => clearInterval(interval)
  })

  return (
    <Grid container spacing={4} className="app-wrapper">
      <Grid item xs={12} content="center">
        <div className="intro">
          Bienvenue sur le site de gestion des bénévoles pour les évènements organisés en Wallonie
        </div>
      </Grid>
      <Grid item md={6} sm={12} xs={12}>
        <Link to="/login:volunteer">
          <Card scale>
            <CardContent>
              <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                Bénévole
              </Typography>
              <Typography variant="h5" component="div">
                Benévole
              </Typography>
            </CardContent>
            <CardActions className="app-wrapper-img">
              <Icon name={images[volunteerImgIndex]} aria-label="Bénévole(s)" />
            </CardActions>
          </Card>
        </Link>
      </Grid>

      <Grid item md={6} sm={12} xs={12}>
        <Link to="/login:manager">
          <Card scale={user?.role !== 0} className={user?.role === 0 ? 'volunteer-permissions' : ''}>
            <CardContent>
              <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                Manager / Administrateur
              </Typography>
              <Typography variant="h5" component="div">
                Manager / Administrateur
              </Typography>
            </CardContent>
            <CardActions className="app-wrapper-img">
              <Icon name="manager" />
            </CardActions>
          </Card>
        </Link>
      </Grid>
    </Grid>
  )
}

export default Home
