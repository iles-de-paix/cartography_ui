import React, { Suspense } from 'react'
import Loader from 'components/Loader/Loader'
import { Grid, Typography } from '@mui/material'
import TabList, { type Tab } from 'components/TabList/TabList'

const LocationMap = React.lazy(() => import('features/LocationMap'))
const CampaignMap = React.lazy(() => import('features/CampaignMap'))
const Users = React.lazy(() => import('features/Users'))
const History = React.lazy(() => import('features/History'))
const Dashboard = React.lazy(() => import('features/Dashboard'))
const DatesForm = React.lazy(() => import('features/DatesForm'))

const tabsManager: Tab[] = [
  { name: 'Lieux', children: <LocationMap /> },
  { name: 'Enregistrements', children: <CampaignMap /> },
  { name: 'Dashboard', children: <Dashboard /> },
  { name: 'Dates', children: <DatesForm /> },
  { name: 'Utilisateurs', children: <Users /> }
]
const tabsAdmin: Tab[] = [
  { name: 'Lieux', children: <LocationMap isAdmin /> },
  { name: 'Enregistrements', children: <CampaignMap isAdmin /> },
  { name: 'Dashboard', children: <Dashboard isAdmin /> },
  { name: 'Dates', children: <DatesForm /> },
  { name: 'Utilisateurs', children: <Users isAdmin /> },
  { name: 'Historique', children: <History /> }
]

const Admin: React.FC<{ isAdmin?: boolean }> = ({ isAdmin = false }) => {
  const tabs = isAdmin ? tabsAdmin : tabsManager

  return (
    <Grid container spacing={2} className="appWrapper">
      <Grid item xs={12}>
        <Typography variant="h3" component="div" className="centerTitle">
          {isAdmin ? 'Administrateur' : 'Manager'}
        </Typography>
        <Suspense fallback={<Loader />}>
          <TabList tabs={tabs} />
        </Suspense>
      </Grid>
    </Grid>
  )
}

export default Admin
