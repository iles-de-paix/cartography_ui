import type { Locale } from 'i18n'

declare module 'i18next' {
  interface CustomTypeOptions {
    resources: { translation: Locale }
  }
}
