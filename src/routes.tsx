import React, { Suspense, useContext } from 'react'
import { Routes as RoutesRouter, Route } from 'react-router-dom'
import { UserContext } from 'providers/UserProvider'
import classNames from 'classnames'

import Header from 'header'
import Loader from 'components/Loader/Loader'

import './index.scss'

const Home = React.lazy(() => import('pages/Home'))
const Login = React.lazy(() => import('pages/Login'))
const Admin = React.lazy(() => import('pages/Admin'))
// const NotFound = React.lazy(() => import('pages/NotFound'))
const Volunteer = React.lazy(() => import('pages/Volunteer'))

const Container: React.FC<{ className?: string; children: React.ReactNode }> = ({ className, children }) => (
  <div className={classNames('containerWrapper', className)}>
    <Header />
    {children}
  </div>
)

const Routes: React.FC = () => {
  const { user } = useContext(UserContext)

  const privateRoutes = () => (
    <>
      <Route
        path="/volunteer"
        element={
          <Container>
            <Volunteer />
          </Container>
        }
      />
      <Route
        path="/admin"
        element={
          <Container>
            <Admin isAdmin />
          </Container>
        }
      />
      <Route
        path="/manager"
        element={
          <Container>
            <Admin />
          </Container>
        }
      />
    </>
  )

  return (
    <Suspense
      fallback={
        <Container>
          <Loader />
        </Container>
      }
    >
      <RoutesRouter>
        <Route
          path="*"
          element={
            <Container>
              <Home />
            </Container>
          }
        />
        <Route
          path="/login:manager"
          element={
            <Container>
              <Login role="manager" />
            </Container>
          }
        />
        <Route
          path="/login:volunteer"
          element={
            <Container>
              <Login role="volunteer" />
            </Container>
          }
        />
        {user?.email && privateRoutes()}
        {/* <Route
          path="*"
          element={
            <Container>
              <NotFound />
            </Container>
          }
        /> */}
      </RoutesRouter>
    </Suspense>
  )
}

export default Routes
