import type { CampaignDate } from 'models/campaign'

const mockCampaignDates: CampaignDate[] = [
  {
    date: '2024-02-16T00:00:00',
    year: '2024',
    id: '1',
    createdAt: '2024-02-16T15:37:12'
  },
  {
    date: '2024-02-17T00:00:00',
    year: '2024',
    id: '2',
    createdAt: '2024-02-16T15:37:12'
  },
  {
    date: '2024-02-18T00:00:00',
    year: '2024',
    id: '3',
    createdAt: '2024-02-16T15:37:12'
  },
  {
    date: '2023-02-18T00:00:00',
    year: '2023',
    id: '4',
    createdAt: '2024-02-18T15:37:12'
  }
]

export default mockCampaignDates
