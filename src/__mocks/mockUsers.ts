import type { User } from 'models/user'

const mockUsers: User[] = [
  {
    id: '1',
    firstName: 'Loic',
    lastName: 'Burnotte',
    email: 'loic.burnotte@devoteam.lu',
    role: 2
  },
  {
    id: '2',
    firstName: 'John',
    lastName: 'Doe',
    email: 'john.doe@gmail.com',
    role: 0
  },
  {
    id: '3',
    firstName: 'Daniel',
    lastName: 'Manager',
    email: 'daniel.manager@gmail.com',
    role: 1
  },
  {
    id: '4',
    firstName: 'Loïc',
    lastName: 'Jacques',
    email: 'jacques_loic@funkyduck.be',
    role: 2
  },
  {
    id: '5',
    firstName: 'Luc',
    lastName: 'Sembourg',
    email: 'l.sembourg@yahoo.com',
    role: 1
  },
  {
    id: '6',
    firstName: 'Jacques',
    lastName: 'Célaire',
    email: 'Jacques_C@hotmail.be',
    role: 0
  },
  {
    id: '7',
    firstName: 'Louis',
    lastName: 'Fine',
    email: 'louisfine@gmail.com',
    role: 0
  },
  {
    id: '8',
    firstName: 'Charles',
    lastName: 'Atan',
    email: 'ch.atan@caramail.com',
    role: 0
  },
  {
    id: '9',
    firstName: 'Edmond',
    lastName: 'Prochain',
    email: 'ep_1945@gmail.com',
    role: 0
  },
  {
    id: '10',
    firstName: 'Jordy',
    lastName: 'Nateur',
    email: 'jordynateur@hotmail.com',
    role: 0
  }
]

export default mockUsers
