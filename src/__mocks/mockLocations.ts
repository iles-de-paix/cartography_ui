import type { Location } from 'models/location'

const mockLocations: Location[] = [
  {
    id: '1',
    name: 'Carrefour Gembloux',
    address_name: 'Adresse du Carrefour',
    address: { country: 'Belgium' },
    place_id: 1234,
    coordinates: {
      lat: 50.60496,
      lon: 3.39124
    }
  },
  {
    id: '2',
    name: 'Delhaize Tournai',
    address_name: "Rue de la tête d'or 22\n7500 Tournai",
    address: { country: 'Belgium' },
    place_id: 1235,
    coordinates: {
      lat: 50.60337,
      lon: 3.40651
    }
  },
  {
    id: '3',
    name: 'Colruyt Namur',
    address_name: 'Adresse Colruyt Namur',
    address: { country: 'Belgium' },
    place_id: 1236,
    coordinates: {
      lat: 50.60405,
      lon: 4.42418
    }
  },
  {
    name: 'Aldi',
    address_name: 'Rue des Bastions 18\n7500 Tournai',
    address: { country: 'Belgium' },
    place_id: 1237,
    coordinates: {
      lat: 50.60046,
      lon: 3.40523
    },
    id: '922e'
  },
  {
    id: '2',
    name: 'Delhaize',
    address_name: 'Boulevard Walter de Marvis 7500\nTournai\n<i>CC Les bastions</i>',
    address: { country: 'Belgium' },
    place_id: 1238,
    coordinates: { lat: 51.60337, lon: 3.70651 }
  }
]

export default mockLocations
