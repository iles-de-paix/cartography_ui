import { useContext, useEffect, useMemo } from 'react'
import { create } from 'zustand'
import { isBefore } from 'date-fns'
import _orderBy from 'lodash/orderBy'
import { useMutation, useQuery } from 'react-query'
import { UserContext } from 'providers/UserProvider'
import type { Registration } from 'models/registration'
import { fetchData, createData, updateData, deleteData, succeededStatus } from 'services/services'

const key = 'Registrations'

const intialState: Registration = {
  id: '',
  amount: 0,
  afternoonSelected: false,
  date: '',
  locationId: '',
  morningSelected: false,
  userId: ''
}

type RegistrationStore = {
  registrations: Registration[]
  setRegistrations: (registrations: Registration[]) => void
}

const useRegistrationStore = create<RegistrationStore>()(set => ({
  registrations: [],
  setRegistrations: registrations => set(() => ({ registrations }))
}))

const today = new Date()

const useRegistrations = () => {
  const { registrations, setRegistrations } = useRegistrationStore()
  const { user } = useContext(UserContext)
  const { data, isLoading, isFetching, isError, status } = useQuery(key, () => fetchData<Registration>(key), {
    cacheTime: 60000, // Cache data for 1 minute
    staleTime: 300000 // Consider data fresh for 5 minutes
  })

  const filteredPastRegistrationsByUser = useMemo(
    () => registrations?.filter(r => !isBefore(today, new Date(r.date)) && r.userId === user?.id),
    [registrations, user]
  )

  const filteredRegistrationsByUser = useMemo(
    () => registrations?.filter(r => r.userId === user?.id) || [],
    [registrations, user]
  )

  useEffect(() => {
    if (!data?.data) return

    const sortedRegistrationsByDate = _orderBy(data.data, 'date')
    setRegistrations(sortedRegistrationsByDate)
  }, [data, setRegistrations])

  const addRegistration = useMutation(
    async (newRegistration: Omit<Registration, 'id' | 'userId'> & { userId?: string }) => {
      if (!user?.id) return

      const _newRegistration: Registration = {
        ...intialState,
        ...newRegistration,
        id: crypto.randomUUID(),
        userId: newRegistration.userId || user.id
      }
      const _response = await createData<Registration>(key, _newRegistration)

      if (succeededStatus.includes(_response.status)) {
        const updatedRegistrations = registrations?.length ? [...registrations, _newRegistration] : [_newRegistration]
        const sortedRegistrationsByDate = _orderBy(updatedRegistrations, 'day')
        setRegistrations(sortedRegistrationsByDate)
      }
    }
  )

  const editRegistration = useMutation(async (updatedRegistration: Registration) => {
    const _response = await updateData<Registration>(key, updatedRegistration)

    if (succeededStatus.includes(_response.status)) {
      const updatedRegistrations = registrations?.map(registration =>
        registration.id === updatedRegistration.id ? updatedRegistration : registration
      )
      setRegistrations(updatedRegistrations)
    }
  })

  const removeRegistration = useMutation(async (registrationId: Registration['id']) => {
    const _response = await deleteData<Registration>(key, registrationId)

    if (succeededStatus.includes(_response.status)) {
      const updatedRegistrations = registrations?.filter(registration => registration.id !== registrationId)
      setRegistrations(updatedRegistrations)
    }
  })

  return {
    registrations,
    filteredRegistrationsByUser,
    filteredPastRegistrationsByUser,
    isLoading,
    isFetching,
    isError,
    status,
    addRegistration,
    removeRegistration,
    editRegistration
  }
}

export default useRegistrations
