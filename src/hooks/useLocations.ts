import { useEffect } from 'react'
import { create } from 'zustand'
import type { Location } from 'models/location'
import { useMutation, useQuery } from 'react-query'
import { createData, updateData, deleteData, fetchFilteredData, succeededStatus } from 'services/services'

const key = 'Locations'

export type LocationFilters = Record<keyof Location['address'], string>

type LocationStore = {
  locations: Location[]
  setLocations: (users: Location[]) => void
}

const useLocationStore = create<LocationStore>()(set => ({
  locations: [],
  setLocations: locations => set(() => ({ locations }))
}))

const useLocations = (filters?: LocationFilters) => {
  const { locations, setLocations } = useLocationStore()
  const { data, isLoading, isFetching, isError, status } = useQuery(
    key,
    () => fetchFilteredData<Location>(key, filters),
    {
      cacheTime: 60000, // Cache data for 1 minute
      staleTime: 300000 // Consider data fresh for 5 minutes
    }
  )

  useEffect(() => {
    if (!data?.data) return

    setLocations(data.data)
  }, [data, setLocations])

  const addLocation = useMutation(async (newLocation: Omit<Location, 'id'>) => {
    const today = new Date().toISOString()
    const _newLocation: Location = {
      ...newLocation,
      id: crypto.randomUUID(),
      createdAt: today,
      modifiedAt: today
    }
    const _response = await createData<Location>(key, _newLocation)

    if (succeededStatus.includes(_response.status)) {
      const updatedLocations = locations?.length ? [...locations, _newLocation] : [_newLocation]
      setLocations(updatedLocations)
    }
  })

  const editLocation = useMutation(async (updatedLocation: Location) => {
    const _updatedLocation = { ...updatedLocation, modifiedAt: new Date().toISOString() }
    const _response = await updateData<Location>(key, _updatedLocation)

    if (succeededStatus.includes(_response.status)) {
      const updatedLocations = locations?.map(location =>
        location.id === updatedLocation.id ? _updatedLocation : location
      )
      setLocations(updatedLocations)
    }
  })

  const removeLocation = useMutation(async (locationId: Location['id']) => {
    const _response = await deleteData<Location>(key, locationId)

    if (succeededStatus.includes(_response.status)) {
      const updatedLocations = locations?.filter(location => location.id !== locationId)
      setLocations(updatedLocations)
    }
  })

  return {
    isLoading,
    isFetching,
    isError,
    status,
    locations,
    addLocation,
    removeLocation,
    editLocation
  }
}

export default useLocations
