import { create } from 'zustand'
import _sortBy from 'lodash/sortBy'
import type { User } from 'models/user'
import { useContext, useEffect } from 'react'
import { useMutation, useQuery } from 'react-query'
import { UserContext, localStorageKey } from 'providers/UserProvider'
import { fetchData, createData, updateData, deleteData, succeededStatus } from 'services/services'

const key = 'Users'

type UserStore = {
  users: User[]
  setUsers: (users: User[]) => void
}

const useUserStore = create<UserStore>()(set => ({
  users: [],
  setUsers: users => set(() => ({ users }))
}))

const useUsers = () => {
  const { users, setUsers } = useUserStore()
  const { setUser } = useContext(UserContext)
  const { data, isLoading, isFetching, isError, status } = useQuery(key, () => fetchData<User>(key))

  useEffect(() => {
    if (!data?.data) return

    const currentUserEmail = localStorage.getItem(localStorageKey)
    if (currentUserEmail) {
      const selectedUser = data.data.find(u => u.email === currentUserEmail)
      setUser(selectedUser)
    }
    const sortedUsers = _sortBy(data.data, ['firstName', 'email'])
    setUsers(sortedUsers)
  }, [data, setUsers, setUser])

  const addUser = useMutation(async (newUser: Omit<User, 'id'>) => {
    const _newUser: User = { ...newUser, id: crypto.randomUUID() }
    const _response = await createData<User>(key, _newUser)

    if (succeededStatus.includes(_response.status)) {
      const updatedUsers = users ? [...users, _newUser] : [_newUser]
      const sortedUsers = _sortBy(updatedUsers, ['firstName', 'email'])
      setUsers(sortedUsers)
    }
  })

  const editUser = useMutation(async (updatedUser: User) => {
    const _response = await updateData<User>(key, updatedUser)

    if (succeededStatus.includes(_response.status)) {
      const updatedUsers = users?.map(user => (user.id === updatedUser.id ? updatedUser : user))
      setUsers(updatedUsers)
    }
  })

  const removeUser = useMutation(async (userId: User['id']) => {
    const _response = await deleteData<User>(key, userId)

    if (succeededStatus.includes(_response.status)) {
      const updatedUsers = users?.filter(user => user.id !== userId)
      setUsers(updatedUsers)
    }
  })

  return {
    users,
    isLoading,
    isFetching,
    isError,
    status,
    addUser,
    removeUser,
    editUser
  }
}

export default useUsers
