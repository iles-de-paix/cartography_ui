import { useEffect, useMemo } from 'react'
import { create } from 'zustand'
import { isBefore } from 'date-fns'
import _sortBy from 'lodash/sortBy'
import type { CampaignDate } from 'models/campaign'
import { useMutation, useQuery } from 'react-query'
import { createData, updateData, deleteData, fetchFilteredData, succeededStatus } from 'services/services'

const key = 'CampaignDates'

const thisYear = new Date().getFullYear().toString()
const today = new Date()

type CampaignDateStore = {
  campaignDates: CampaignDate[]
  setCampaignDates: (campaignDates: CampaignDate[]) => void
}

const useCampaignDateStore = create<CampaignDateStore>()(set => ({
  campaignDates: [],
  setCampaignDates: campaignDates => set(() => ({ campaignDates }))
}))

const useCampaignDates = (allYears: boolean = false) => {
  const { campaignDates, setCampaignDates } = useCampaignDateStore()
  const { data, isLoading, isFetching, isError, status } = useQuery(
    key,
    () => fetchFilteredData<CampaignDate>(key, allYears ? undefined : { year: thisYear }),
    {
      cacheTime: 60000, // Cache data for 1 minute
      staleTime: 300000 // Consider data fresh for 5 minutes
    }
  )

  const filteredCampaignDates = useMemo(
    () => campaignDates?.filter(d => !isBefore(new Date(d.date), today)),
    [campaignDates]
  )

  useEffect(() => {
    if (!data?.data) return

    const sortedCampaignsByDates = _sortBy(data.data, 'date')
    setCampaignDates(sortedCampaignsByDates)
  }, [data, setCampaignDates])

  const addCampaignDate = useMutation(async (newCampaignDate: Omit<CampaignDate, 'id'>) => {
    const _newCampaignDate: CampaignDate = { ...newCampaignDate, id: crypto.randomUUID() }
    const _response = await createData<CampaignDate>(key, _newCampaignDate)

    if (succeededStatus.includes(_response.status)) {
      const updatedCampaignDates = campaignDates?.length ? [...campaignDates, _newCampaignDate] : [_newCampaignDate]
      const sortedCampaignsByDates = _sortBy(updatedCampaignDates, 'date')
      setCampaignDates(sortedCampaignsByDates)
    }
  })

  const editCampaignDate = useMutation(async (updatedCampaignDate: CampaignDate) => {
    const _response = await updateData<CampaignDate>(key, updatedCampaignDate)

    if (succeededStatus.includes(_response.status)) {
      const updatedCampaignDates = campaignDates?.map(campaignDate =>
        campaignDate.id === updatedCampaignDate.id ? updatedCampaignDate : campaignDate
      )
      setCampaignDates(updatedCampaignDates)
    }
  })

  const removeCampaignDate = useMutation(async (campaignDateId: CampaignDate['id']) => {
    const _response = await deleteData<CampaignDate>(key, campaignDateId)

    if (succeededStatus.includes(_response.status)) {
      const updatedCampaignDates = campaignDates?.filter(campaignDate => campaignDate.id !== campaignDateId)
      setCampaignDates(updatedCampaignDates)
    }
  })

  return {
    campaignDates,
    filteredCampaignDates,
    isLoading,
    isFetching,
    isError,
    status,
    addCampaignDate,
    removeCampaignDate,
    editCampaignDate
  }
}

export default useCampaignDates
