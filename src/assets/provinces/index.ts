// INFO: all json files are provided by this link => https://www.odwb.be/explore/dataset/communesgemeente-belgium/information/
// provinces
import Liege from './province-de-liege.json'
import Namur from './province-de-namur.json'
import BrabantWallon from './province-du-brabant-wallon.json'
import Hainaut from './province-du-hainaut.json'
import Luxembourg from './province-du-luxembourg.json'
import Bruxelles from './bruxelles-capitale.json'

// regions
import Wallonie from './region-wallone.json'
import Flandre from './region-flamande.json'

export { Liege, Namur, BrabantWallon, Hainaut, Luxembourg, Bruxelles, Wallonie, Flandre }

const provinces = { Liege, Namur, BrabantWallon, Hainaut, Luxembourg, Bruxelles, Wallonie, Flandre } as const
export default provinces
