import React, { Suspense, useContext, useMemo } from 'react'
import _uniqBy from 'lodash/uniqBy'
import TabList from 'components/TabList'
import { Icon } from 'components/Icon/Icon'
import Loader from 'components/Loader/Loader'
import { Grid, Checkbox, FormGroup, FormControlLabel, Typography } from '@mui/material'

import { ChosenTheme } from 'providers'
import { getCompleteDate } from 'helpers/date.helper'
import useRegistrations from 'hooks/useRegistrations'
import type { Tab } from 'components/TabList/TabList'
import useCampaignDates from 'hooks/useCampaignDates'
import { getGroupedCampaignDatesByCreatedDate } from 'helpers/helper'

import './VolunteerDates.scss'

export const campaignColors = [
  '#16537e',
  '#f6b26b',
  '#f44336',
  '#6aa84f',
  '#e6e6fa',
  '#1e4c6d',
  '#ce7e00',
  '	#994570',
  '#174005',
  '#999999'
]

const VolunteerLocationForm = React.lazy(() => import('features/VolunteerLocationForm'))

const VolunteerDates: React.FC = () => {
  const { theme } = useContext(ChosenTheme)
  const { campaignDates, isLoading, isFetching } = useCampaignDates()
  const { filteredRegistrationsByUser, removeRegistration, addRegistration } = useRegistrations()

  const filteredDates: Tab[] = useMemo(
    () =>
      _uniqBy(
        filteredRegistrationsByUser.map(registration => ({
          name: getCompleteDate(registration.date),
          children: <VolunteerLocationForm registration={registration} />
        })),
        'name'
      ),
    [filteredRegistrationsByUser]
  )

  const campaignDatesgroupedByCreatedDate = useMemo(
    () => getGroupedCampaignDatesByCreatedDate(campaignDates),
    [campaignDates]
  )

  const handleChange = (checked: boolean, _date: string) => {
    if (!checked) {
      const id = filteredRegistrationsByUser.find(r => r.date === _date)?.id
      id && removeRegistration.mutate(id)
    } else {
      addRegistration.mutate({
        date: _date
      })
    }
  }

  if (isLoading || isFetching) return <Loader />

  return (
    <Grid item xs={12}>
      <Typography variant="h5" color="text.secondary" gutterBottom>
        Sélectionnez vos disponibilités:
      </Typography>

      <div className="volunteer-dates-group">
        <FormGroup>
          {Object.entries(campaignDatesgroupedByCreatedDate).map(([, campaigns], i) => {
            const color = `${campaignColors[i % 10]}25`

            return campaigns?.map(campaign => {
              const { date, id } = campaign
              const _date = getCompleteDate(date)

              return (
                <FormControlLabel
                  key={id}
                  checked={filteredDates.map(s => s.name).includes(_date)}
                  onChange={(_event, checked) => handleChange(checked, date)}
                  control={<Checkbox />}
                  className="list"
                  label={_date}
                  style={{ backgroundColor: color, borderRadius: '4px', margin: '3px' }}
                />
              )
            })
          })}
        </FormGroup>
        <Icon
          name="calendar"
          width={165}
          className={`${theme === 'dark' ? 'volunteer-dates-img-dark' : 'volunteer-dates-img-light'} volunteer-dates-img`}
        />
      </div>

      <Suspense fallback={<Loader />}>
        <TabList tabs={filteredDates} />
      </Suspense>
    </Grid>
  )
}

export default VolunteerDates
