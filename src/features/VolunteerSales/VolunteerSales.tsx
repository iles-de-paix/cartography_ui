import React, { useEffect, useState } from 'react'

import { useForm } from 'react-hook-form'
import useLocations from 'hooks/useLocations'
import { getRangeHours } from 'helpers/helper'
import { getCompleteDate } from 'helpers/date.helper'
import useRegistrations from 'hooks/useRegistrations'
import type { Registration } from 'models/registration'

import { Icon } from 'components/Icon/Icon'
import Loader from 'components/Loader/Loader'
import SaveIcon from '@mui/icons-material/Save'
import EuroIcon from '@mui/icons-material/Euro'
import { Grid, Typography, TextField, InputAdornment, Button } from '@mui/material'

import './VolunteerSales.scss'

const VolunteerSales: React.FC = () => {
  const { locations } = useLocations()
  const { filteredRegistrationsByUser, editRegistration, isLoading, isFetching } = useRegistrations()
  const { handleSubmit } = useForm<Registration>()

  const [updatedRegistrations, setUpdatedRegistrations] = useState<Registration[]>([])

  useEffect(() => {
    if (filteredRegistrationsByUser.length) setUpdatedRegistrations(filteredRegistrationsByUser)
  }, [filteredRegistrationsByUser])

  const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, registration: Registration) => {
    setUpdatedRegistrations(prevState => {
      const index = prevState.findIndex(s => s.id === registration.id)
      if (index === -1) return prevState
      const newUpdatedRegistration = prevState
      newUpdatedRegistration[index] = { ...registration, amount: Number(e.target.value) }
      return newUpdatedRegistration
    })
  }

  const handleSubmitForm = () => {
    updatedRegistrations.map(registration => {
      // if (registration.amount !== registrations.find(r => r.id === registration.id)?.amount) {
      editRegistration.mutate(registration)
    })
  }

  if (isLoading || isFetching) return <Loader />

  return (
    <form onSubmit={handleSubmit(handleSubmitForm)}>
      <Grid item xs={12} className="volunteer-sales">
        <Typography variant="h5" color="text.secondary" gutterBottom>
          Encodez vos ventes:
        </Typography>
        {!!updatedRegistrations.length ? (
          <ul>
            {updatedRegistrations.map(registration => {
              const { id, amount, date, locationId } = registration
              const location = locations.find(l => l.id === locationId)
              return (
                <li key={id}>
                  <div className="date">
                    <span>{getCompleteDate(date)}:</span>
                    <span style={{ color: 'grey' }}>
                      <i>{getRangeHours(registration.morningSelected, registration.afternoonSelected)}</i>
                    </span>
                  </div>
                  <TextField
                    id="outlined-number"
                    defaultValue={amount}
                    type="number"
                    InputLabelProps={{
                      shrink: true
                    }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <EuroIcon />
                        </InputAdornment>
                      )
                    }}
                    onChange={e => handleChange(e, registration)}
                  />
                  {location && <span className="address">{`${location.name} (${location.address_name})`}</span>}
                </li>
              )
            })}
          </ul>
        ) : (
          <Icon name="no_data" className="noTabImg" width={220} />
        )}

        <Button type="submit" variant="contained" className="save-button">
          <SaveIcon />
          &nbsp;Enregistrer
        </Button>
        <Icon name="sales" width={197} className="volunteer-sales-img" />
      </Grid>
    </form>
  )
}

export default VolunteerSales
