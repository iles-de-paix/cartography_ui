import React, { useMemo } from 'react'
import useLocations from 'hooks/useLocations'
import type { CampaignDate } from 'models/campaign'
import useRegistrations from 'hooks/useRegistrations'
import useCampaignDates from 'hooks/useCampaignDates'
import { getCompleteDate } from 'helpers/date.helper'
import type { Registration } from 'models/registration'
import { useForm, type SubmitHandler, Controller, useWatch } from 'react-hook-form'

import {
  Grid,
  Select,
  Button,
  MenuItem,
  Checkbox,
  TextField,
  InputLabel,
  FormControl,
  Autocomplete,
  InputAdornment,
  FormHelperText,
  FormControlLabel
} from '@mui/material'
import Snackbar from 'components/Snackbar'
import AddIcon from '@mui/icons-material/Add'
import EuroIcon from '@mui/icons-material/Euro'
import SaveIcon from '@mui/icons-material/Save'

import './RegistrationForm.scss'

export interface RegistrationFormProps {
  registration?: Registration
  userId?: string
  onClose: () => void
}

const RegistrationForm: React.FC<RegistrationFormProps> = ({ registration, userId, onClose }) => {
  const { locations } = useLocations()
  const { campaignDates } = useCampaignDates(true)
  const { editRegistration, addRegistration } = useRegistrations()

  const {
    watch,
    control,
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<Registration>({
    defaultValues: { ...registration }
  })

  const allCampaignDates = useMemo<CampaignDate[]>(
    () =>
      registration?.date
        ? [
            {
              id: '0',
              date: registration.date,
              createdAt: new Date().toISOString(),
              year: new Date(registration.date).getUTCFullYear().toString()
            },
            ...campaignDates
          ]
        : campaignDates,
    [campaignDates, registration]
  )

  const watchedLocationIdField = useWatch({
    control,
    name: 'locationId'
  })

  const selectedLocation = useMemo(() => {
    const _locationId = watchedLocationIdField || registration?.locationId
    if (!locations?.length || !_locationId) return

    const _location = locations.find(l => l.id === _locationId)
    if (!_location) return

    return {
      label: `${_location.name} (${_location.address_name})`,
      key: _location.id
    }
  }, [locations, registration, watchedLocationIdField])

  const onSubmit: SubmitHandler<Registration> = (data: Registration | Omit<Registration, 'id'>) => {
    'id' in data
      ? editRegistration.mutate({ ...data, amount: data.amount ? Number(data.amount) : 0 })
      : addRegistration.mutate({ ...data, userId, amount: data.amount ? Number(data.amount) : 0 })
    onClose()
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="registration-form" key={crypto.randomUUID()}>
      <Grid container spacing={2}>
        {!!allCampaignDates?.length && (
          <Grid item sm={6} xs={12}>
            <FormControl fullWidth>
              <InputLabel id="select-date-label">Date</InputLabel>
              <Select
                label="Date"
                labelId="select-date-label"
                inputProps={{ 'aria-label': 'date' }}
                error={!!errors?.date}
                value={registration?.date || watch('date') || allCampaignDates[0].date}
                {...register('date', { required: true })}
              >
                {allCampaignDates.map(c => (
                  <MenuItem key={c.id} value={c.date}>
                    {getCompleteDate(c.date)}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
        )}

        <Grid item sm={6} xs={12}>
          <FormControl error={!!errors?.locationId} fullWidth>
            <Controller
              name="locationId"
              control={control}
              defaultValue=""
              render={props => (
                <Autocomplete
                  disablePortal
                  autoHighlight
                  noOptionsText="Aucune adresse"
                  onChange={(_event, option) => props.field.onChange(option?.key)}
                  getOptionLabel={option => option.label}
                  getOptionKey={option => option.key}
                  isOptionEqualToValue={(option, value) => option.key === value.key}
                  defaultValue={selectedLocation ?? undefined}
                  options={
                    !!locations.length
                      ? locations.map(l => ({ label: `${l.name} (${l.address_name})`, key: l.id }))
                      : selectedLocation
                        ? [selectedLocation]
                        : []
                  }
                  renderInput={params => (
                    <TextField
                      {...params}
                      required
                      label="Lieu"
                      InputLabelProps={{
                        shrink: true
                      }}
                      inputRef={() => register('locationId', { required: true })}
                    />
                  )}
                />
              )}
            />
            {errors?.locationId && (
              <FormHelperText id="locationId-error" error>
                Le lieu est obligatoire
              </FormHelperText>
            )}
          </FormControl>
        </Grid>

        <Grid item sm={6} xs={12} className="section">
          <FormControlLabel
            label="Matin"
            control={<Checkbox checked={watch('morningSelected')} />}
            {...register('morningSelected')}
          />

          <FormControlLabel
            label="Après-midi"
            control={<Checkbox checked={watch('afternoonSelected')} />}
            {...register('afternoonSelected')}
          />
        </Grid>

        <Grid item sm={6} xs={12} className="section">
          <TextField
            id="outlined-number"
            type="number"
            InputLabelProps={{
              shrink: true
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <EuroIcon />
                </InputAdornment>
              )
            }}
            {...register('amount')}
          />
        </Grid>

        <Grid item sm={6} xs={12}>
          <Button
            type="submit"
            variant="contained"
            className="save-btn"
            disabled={editRegistration.status === 'loading'}
          >
            {registration ? (
              <>
                <SaveIcon />
                &nbsp;Enregistrer
              </>
            ) : (
              <>
                <AddIcon />
                &nbsp;Ajouter
              </>
            )}
          </Button>
        </Grid>
      </Grid>
      {editRegistration.status === 'error' && <Snackbar isOpen message="Enregistrement échoué" />}
      {editRegistration.status === 'success' && <Snackbar status="success" isOpen message="Mis à jour" />}
    </form>
  )
}

export default RegistrationForm
