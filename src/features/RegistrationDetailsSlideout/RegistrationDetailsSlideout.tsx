import React, { useState, type ComponentProps } from 'react'
import Card from 'components/Card'
import Modal from 'components/Modal'
import type { User } from 'models/user'
import Icon from 'components/Icon/Icon'
import Slideout from 'components/Slideout'
import useLocations from 'hooks/useLocations'
import { Button, Typography } from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete'
import useRegistrations from 'hooks/useRegistrations'
import { getCompleteDate } from 'helpers/date.helper'
import type { Registration } from 'models/registration'
import RegistrationForm from 'features/RegistrationForm'
import { getFormattedCurrency, getRangeHours, getRoleName } from 'helpers/helper'

import './RegistrationDetailsSlideout.scss'

export interface RegistrationDetailsSlideoutProps extends ComponentProps<typeof Slideout> {
  selectedUserIds: User['id'][]
  users?: User[]
}

const RegistrationDetailsSlideout: React.FC<RegistrationDetailsSlideoutProps> = ({
  selectedUserIds,
  users,
  ...props
}) => {
  const { locations } = useLocations()
  const { registrations, removeRegistration } = useRegistrations()

  const [isOpen, setIsOpen] = useState(false)
  const [selectedUserId, setSelectedUserId] = useState<string>()
  const [selectedRegistration, setSelectedRegistration] = useState<Registration>()

  const handleClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>, registration: Registration) => {
    e.preventDefault()
    setSelectedRegistration(registration)
    setIsOpen(i => !i)
  }

  const addRegistration = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, userId: string) => {
    e.preventDefault()
    setIsOpen(true)
    setSelectedUserId(userId)
  }

  const deleteRegistration = (e: React.MouseEvent<SVGSVGElement, MouseEvent>, id: string) => {
    e.preventDefault()
    removeRegistration.mutate(id)
  }

  return (
    <Slideout {...props} className="registrations-slideout">
      <Modal open={isOpen} onClose={() => setIsOpen(false)}>
        <RegistrationForm
          registration={selectedRegistration}
          onClose={() => setIsOpen(false)}
          userId={selectedUserId}
        />
      </Modal>
      <Typography variant="h4" color="text.secondary" gutterBottom>
        Détails des enregistrements
      </Typography>
      {selectedUserIds?.map(id => {
        const user = users?.find(u => u.id === id)
        const filteredRegistrations = registrations.filter(r => r.userId === id)
        const role = getRoleName(user?.role)
        return (
          <Card key={id} className="registrations-slideout-card">
            <Typography
              variant="h6"
              color="text.secondary"
              gutterBottom
            >{`${user?.firstName} ${user?.lastName} (${user?.email}) - ${role}`}</Typography>
            <>
              {filteredRegistrations?.length ? (
                filteredRegistrations.map(r => {
                  const location = locations.find(l => l.id === r.locationId)
                  const date = getCompleteDate(r.date)
                  return (
                    <Card key={r.id} className="registrations-slideout-card__subcard" onClick={e => handleClick(e, r)}>
                      <div>
                        <b>{`${date} - (${getRangeHours(r.morningSelected, r.afternoonSelected)})`}</b>
                      </div>
                      <div>
                        <b>Total des ventes:</b>
                        {` ${getFormattedCurrency(r.amount)}`}
                      </div>
                      <div>
                        <b>Adresse:</b>
                        {` ${location?.name} (${location?.address_name})`}
                      </div>
                      <DeleteIcon color="primary" className="delete-btn" onClick={e => deleteRegistration(e, r.id)} />
                    </Card>
                  )
                })
              ) : (
                <div className="no-registrations">
                  <span>Aucun enregistrement</span>
                  <Icon name="no_data" width={120} />
                  <Button variant="contained" size="small" onClick={e => addRegistration(e, id)}>
                    Ajouter un nouvel enregistrement
                  </Button>
                </div>
              )}
            </>
          </Card>
        )
      })}
    </Slideout>
  )
}

export default RegistrationDetailsSlideout
