import React, { useState, useMemo } from 'react'
import { t } from 'i18next'
import UserForm from '../UserForm'
import Modal from 'components/Modal'
import useUsers from 'hooks/useUsers'
import { getRoleName } from 'helpers/helper'
import Loader from 'components/Loader/Loader'
import AddIcon from '@mui/icons-material/Add'
import EditIcon from '@mui/icons-material/Edit'
import { Box, Button, Grid } from '@mui/material'
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline'
import RegistrationDetailsSlideout from 'features/RegistrationDetailsSlideout'
import { DataGrid, type GridRowSelectionModel, type GridColDef } from '@mui/x-data-grid'

import type { User } from 'models/user'
import type { Province } from 'models/location'

import './Users.scss'

const cUsers: GridColDef[] = [
  { field: 'firstName', headerName: 'Prénom', width: 120 },
  { field: 'lastName', headerName: 'Nom', width: 120 },
  { field: 'email', headerName: 'Email', width: 200 },
  {
    field: 'role',
    headerName: 'Role',
    width: 100,
    renderCell: (cell: { row: { role: number } }) => {
      return <span>{getRoleName(cell.row.role)}</span>
    }
  },
  {
    field: 'phoneNumber',
    headerName: 'Numéro de téléphone',
    width: 200
  },
  {
    field: 'provinces',
    headerName: 'Province(s)',
    width: 300,
    renderCell: (cell: { row: { provinces: Province[] } }) => {
      return (
        <span>{(cell.row.provinces || [])?.map(province => t(`province.${province}`) || province)?.join(', ')}</span>
      )
    }
  }
]

const DrawTable: React.FC<{ users?: User[]; setSelectedUserIds: (ids: string[]) => void }> = ({
  users = [],
  setSelectedUserIds
}) => {
  if (!users) return null
  return (
    <>
      <Box sx={{ height: 400, width: '100%' }}>
        <DataGrid
          rows={users}
          columns={cUsers}
          autoPageSize
          checkboxSelection
          localeText={{
            footerRowSelected(count) {
              return `${count} utilisateur(s) sélectionné(s)`
            }
          }}
          onRowSelectionModelChange={(rowSelectionModel: GridRowSelectionModel) => {
            setSelectedUserIds(rowSelectionModel.map(i => i.toString()))
          }}
        />
      </Box>
    </>
  )
}

const Users: React.FC<{ isAdmin?: boolean }> = ({ isAdmin = false }) => {
  const { users: allUsers, status, addUser, editUser, removeUser, isLoading, isFetching } = useUsers()

  const [isOpen, setIsOpen] = useState(false)
  const [openDetailsFlyout, setOpenDetailsFlyout] = useState(false)
  const [selectedUserIds, setSelectedUserIds] = useState<string[]>([])

  const users = useMemo(() => (isAdmin ? allUsers : allUsers.filter(u => u.role !== 2)), [allUsers, isAdmin])

  const selectedUser = useMemo(() => {
    if (selectedUserIds?.length !== 1) return undefined
    return users?.find(u => u.id === selectedUserIds[0])
  }, [selectedUserIds, users])

  const deleteUsers = () => {
    selectedUserIds.map(async (id: string) => {
      return removeUser.mutate(id)
    })
  }

  const handleSubmitAndClose = (_user: Omit<User, 'id'> | User) => {
    if ('id' in _user && _user?.id) {
      editUser.mutate(_user as User)
    } else {
      addUser.mutate({
        email: _user.email,
        firstName: _user.firstName,
        lastName: _user.lastName,
        phoneNumber: _user.phoneNumber,
        role: _user.role,
        provinces: _user.provinces
      })
    }
    setIsOpen(false)
  }

  if (isLoading || isFetching) return <Loader />

  return (
    <Grid container spacing={1}>
      <RegistrationDetailsSlideout
        isOpen={openDetailsFlyout}
        onClose={() => setOpenDetailsFlyout(false)}
        selectedUserIds={selectedUserIds}
        users={users}
      />
      <Grid item xs={12}>
        {status === 'error' && <p>Error fetching data</p>}
        {status === 'success' && users && <DrawTable users={users} setSelectedUserIds={setSelectedUserIds} />}
      </Grid>

      <Grid item xs={12} className="user-btns-container">
        <Button onClick={() => setIsOpen(i => !i)} variant="contained" className="user-btns">
          {!!selectedUser ? (
            <span>Modifier un utilisateur</span>
          ) : (
            <>
              <AddIcon />
              <span>Ajouter un utilisateur</span>
            </>
          )}
        </Button>
        {!!selectedUserIds?.length && (
          <Button onClick={() => setOpenDetailsFlyout(true)} variant="contained" className="user-btns">
            <>
              <span>Détails</span>
              <EditIcon />
            </>
          </Button>
        )}
        {!!selectedUserIds.length && (
          <Button onClick={deleteUsers} variant="contained" className="user-btns">
            <DeleteOutlineIcon />
            <span>Supprimer</span>
          </Button>
        )}
      </Grid>

      <Grid item xs={12}>
        <Modal onClose={() => setIsOpen(false)} open={isOpen}>
          <UserForm isAdmin={isAdmin} user={selectedUser} handleSubmitAndClose={handleSubmitAndClose} />
        </Modal>
      </Grid>
    </Grid>
  )
}

export default Users
