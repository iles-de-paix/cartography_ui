import React from 'react'
import { t } from 'i18next'
import { provinces } from 'models/location'
import { useForm, type SubmitHandler } from 'react-hook-form'
import { TextField, Button, FormControl, InputLabel, MenuItem, Select } from '@mui/material'

import type { User } from 'models/user'

import './UserForm.scss'

const notAdminRoles = [
  { name: 'Bénévole', id: 0 },
  { name: 'Manager', id: 1 }
]

const allRoles = [...notAdminRoles, { name: 'Administrateur', id: 2 }]

interface UserFormProps {
  isAdmin?: boolean
  user?: User
  handleSubmitAndClose: (user: Omit<User, 'id'> | User) => void
}

const UserForm: React.FC<UserFormProps> = ({ isAdmin = false, user, handleSubmitAndClose }) => {
  const {
    watch,
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<Omit<User, 'id'> | User>({
    defaultValues: { ...user }
  })
  const roles = isAdmin ? allRoles : notAdminRoles

  const onSubmit: SubmitHandler<Omit<User, 'id'> | User> = data => {
    handleSubmitAndClose(data)
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="userAddForm">
      <TextField id="outlined-textarea" label="Prénom" placeholder="Prénom" {...register('firstName')} />
      <TextField id="outlined-textarea" label="Nom" placeholder="Nom" {...register('lastName')} />
      <TextField
        id="outlined-textarea"
        label="Email"
        placeholder="Email"
        helperText={!!errors.email && "L'email est obligatoire"}
        error={!!errors.email}
        required
        type="email"
        autoComplete={'on'}
        {...register('email')}
      />
      <TextField
        id="outlined-textarea"
        label="Numéro de téléphone"
        placeholder="Numéro de téléphone"
        {...register('phoneNumber')}
      />
      <FormControl error={!!errors?.role} fullWidth>
        <InputLabel shrink id="role-label">
          Rôle
        </InputLabel>
        <Select
          aria-describedby="role-error"
          labelId="role-label"
          label="Rôle"
          inputProps={{ 'aria-label': 'role' }}
          error={!!errors?.role}
          value={watch('role') || user?.role || 0}
          {...register('role', { required: true })}
        >
          {roles.map(option => (
            <MenuItem key={option.id} value={option.id}>
              {option.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      <FormControl error={!!errors?.provinces} fullWidth>
        <InputLabel shrink id="provinces-label">
          Province(s) en charge
        </InputLabel>
        <Select
          aria-describedby="provinces-error"
          labelId="provinces-label"
          label="Province(s) en charge"
          multiple
          inputProps={{ 'aria-label': 'provinces' }}
          error={!!errors?.role}
          value={watch('provinces') || user?.provinces || []}
          {...register('provinces')}
        >
          {provinces?.map(option => (
            <MenuItem key={option} value={option}>
              {t(`province.${option}`)}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      <Button type="submit" variant="contained">
        {user?.id ? 'Sauvegarder' : 'Ajouter'}
      </Button>
    </form>
  )
}

export default UserForm
