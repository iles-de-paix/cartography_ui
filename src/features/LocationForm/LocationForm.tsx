import React, { useEffect, useState } from 'react'
import Modal from 'components/Modal'
import SaveIcon from '@mui/icons-material/Save'
import { searchLocation } from 'helpers/helper'
import { useDebounce } from 'helpers/useDebounce'
import type { Location, StreetMapLocation } from 'models/location'
import { useForm, type SubmitHandler, Controller } from 'react-hook-form'
import { TextField, Button, Grid, Autocomplete, FormHelperText, FormControl } from '@mui/material'

import './LocationForm.scss'

export interface LocationFormProps {
  location?: Location
  isOpen: boolean
  onClose: () => void
  onSubmit: (location: Omit<Location, 'id'> & { id?: string }) => void
}

const LocationForm: React.FC<LocationFormProps> = ({ location, isOpen, onClose, onSubmit }) => {
  const {
    control,
    register,
    setValue,
    clearErrors,
    handleSubmit,
    formState: { errors }
  } = useForm<Location>({
    defaultValues: { ...location }
  })

  const [searchTerm, setSearchTerm] = useState('')
  const debouncedValue = useDebounce<string>(searchTerm, 500)
  const [searchResult, setSearchResult] = useState<StreetMapLocation[]>([])

  const hancleClear = () => {
    clearErrors('address_name')
    setSearchTerm('')
    setSearchResult([])
  }

  const handleSubmitForm: SubmitHandler<Location> = (newLocation: Location) => {
    const newAddress: StreetMapLocation | undefined = searchResult.find(
      f => f.display_name === newLocation?.address_name
    )
    const address = newAddress?.address || location?.address
    const address_name = newLocation?.address_name || location?.address_name

    if (!address || !address_name) return

    onSubmit({
      id: location?.id ?? undefined,
      name: newLocation.name || location?.name,
      address_name,
      address,
      osm_id: newAddress?.osm_id || location?.osm_id,
      place_id: newAddress?.place_id || location?.place_id,
      coordinates:
        newAddress?.lat && newAddress?.lon
          ? { lat: Number(newAddress.lat), lon: Number(newAddress.lon) }
          : location?.coordinates
    })
    hancleClear()
  }

  useEffect(() => {
    if (!debouncedValue) return
    searchLocation(debouncedValue).then(result => {
      if (!result) return
      setSearchResult(result)
    })
  }, [debouncedValue])

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    e.preventDefault()
    setSearchTerm(e.target.value)
  }

  const handleClose = () => {
    onClose()
    hancleClear()
  }

  return (
    <Modal open={isOpen} onClose={handleClose}>
      <form onSubmit={handleSubmit(handleSubmitForm)}>
        <Grid container className="location-form">
          <Grid item xs={12}>
            <FormControl error={!!errors?.address_name} fullWidth className="location-address-name">
              <Controller
                name="address_name"
                control={control}
                defaultValue=""
                render={props => (
                  <Autocomplete
                    onChange={(_event, option) => props.field.onChange(option?.label)}
                    autoHighlight
                    defaultValue={
                      !searchResult.length && location?.address_name
                        ? { label: location.address_name, key: location?.place_id || 0 }
                        : undefined
                    }
                    disablePortal
                    noOptionsText="Aucune adresse"
                    getOptionLabel={option => option.label}
                    getOptionKey={option => option.key}
                    isOptionEqualToValue={(option, value) => option.key === value.key && option.label == value.label}
                    options={
                      !searchResult.length && location?.address_name
                        ? [{ label: location.address_name, key: location?.place_id || 0 }]
                        : searchResult?.map(o => ({ label: o.display_name, key: o.place_id }))
                    }
                    renderInput={params => (
                      <TextField
                        {...params}
                        error={!!errors.address_name}
                        required
                        label="Adresse"
                        onChange={e => handleSearch(e)}
                      />
                    )}
                  />
                )}
              />
              {errors?.address_name && <FormHelperText error>Entrez une adresse</FormHelperText>}
            </FormControl>
          </Grid>

          <Grid item xs={12}>
            <TextField
              className="location-name"
              defaultValue={location?.name}
              inputRef={() => register('name')}
              label="Nom"
              onChange={e => setValue('name', e.target.value)}
              placeholder="Nom"
            />
          </Grid>

          <Button type="submit" variant="contained" className="save-button">
            <SaveIcon className="save" />
            &nbsp;
            {location?.id ? 'Enregistrer' : 'Ajouter'}
          </Button>
        </Grid>
      </form>
    </Modal>
  )
}

export default LocationForm
