import React, { useMemo } from 'react'
import useLocations from 'hooks/useLocations'
import { differenceInMinutes } from 'date-fns'

import Map from 'components/Map'
import { Grid } from '@mui/material'
import Loader from 'components/Loader/Loader'
import type { LatLngExpression } from 'leaflet'
import LocationList from 'features/LocationList/LocationList'
import LocationMarkers from 'features/Markers/LocationMarkers'

import './LocationMap.scss'

export interface LocationMapProps {
  isAdmin?: boolean
}

const LocationMap: React.FC<LocationMapProps> = ({ isAdmin = false }) => {
  const { locations, isLoading, isFetching, editLocation } = useLocations()

  const defaultCenter: LatLngExpression = useMemo(
    () => (isAdmin ? [50.275065, 4.513755] : [50.6112, 3.5012]),
    [isAdmin]
  )

  const center: LatLngExpression | undefined = useMemo(() => {
    if (!locations) return defaultCenter

    const latestCreatedLocation = locations?.find(l => {
      if (!l?.modifiedAt) return
      const today = new Date()
      const newDate = new Date(l.modifiedAt)
      const diff = differenceInMinutes(today, newDate)
      if (diff === 0) return l
    })

    return latestCreatedLocation?.coordinates
      ? [latestCreatedLocation.coordinates.lat, latestCreatedLocation.coordinates.lon]
      : undefined
  }, [defaultCenter, locations])

  if (isLoading || isFetching) return <Loader />

  return (
    <Grid container spacing={2} className="location-map-container">
      <Grid item lg={4} xs={12}>
        <LocationList />
      </Grid>
      <Grid item lg={8} xs={12}>
        <Map center={center} zoom={isAdmin ? 8 : 9}>
          <LocationMarkers locations={locations} edit={editLocation} />
        </Map>
      </Grid>
    </Grid>
  )
}

export default LocationMap
