import React, { useEffect, useState } from 'react'
import Card from 'components/Card'
import useLocations from 'hooks/useLocations'
import AddIcon from '@mui/icons-material/Add'
import EditIcon from '@mui/icons-material/Edit'
import type { Location } from 'models/location'
import LocationForm from 'features/LocationForm'
import DeleteIcon from '@mui/icons-material/Delete'
import { Button, CardContent, TextField } from '@mui/material'

import './LocationList.scss'

const LocationList: React.FC = () => {
  const { locations, removeLocation, editLocation, addLocation } = useLocations()
  const [filteredLocations, setFilteredLocations] = useState<Location[]>([])
  const [isOpen, setIsOpen] = useState(false)

  useEffect(() => {
    if (locations?.length) setFilteredLocations(locations)
  }, [locations])

  const [selectedLocation, setSelectedLocation] = useState<Location | undefined>()

  const handleAddLocation = () => {
    setSelectedLocation(undefined)
    setIsOpen(true)
  }

  const handleEditLocation = (location: Location) => {
    setSelectedLocation(location)
    setIsOpen(true)
  }

  const handleDeleteLocation = (locationId: Location['id']) => {
    removeLocation.mutate(locationId)
  }

  const handleSubmit = (location: Location | Omit<Location, 'id'>) => {
    'id' in location && location.id ? editLocation.mutate(location) : addLocation.mutate(location)
    setSelectedLocation(undefined)
    setIsOpen(false)
  }

  const handleClose = () => {
    setSelectedLocation(undefined)
    setIsOpen(false)
  }

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    e.preventDefault()
    const _searchValue = e.target.value.toLowerCase()
    const newFilteredLocations: Location[] =
      locations?.filter(loc => loc.name?.toLowerCase().includes(_searchValue)) || []
    setFilteredLocations(newFilteredLocations)
  }

  return (
    <div className="list-container">
      <LocationForm location={selectedLocation} isOpen={isOpen} onClose={handleClose} onSubmit={handleSubmit} />

      <Button onClick={handleAddLocation} variant="contained" className="add-location-btn">
        <AddIcon />
        &nbsp; Ajouter un lieu
      </Button>

      <TextField label="Adresse" placeholder="Adresse" onChange={e => handleSearch(e)} className="search-field" />

      <Card className="list">
        <CardContent>
          {filteredLocations.map(location => {
            const { id, name, address_name } = location
            return (
              <div className="address" key={id}>
                <div className="address-name">
                  {name}
                  &nbsp;
                  <EditIcon
                    onClick={() => handleEditLocation(location)}
                    fontSize="small"
                    color="primary"
                    className="edit-icon"
                  />
                </div>
                <div dangerouslySetInnerHTML={{ __html: `<b>Adresse:</b> <i>${address_name}</i>` }} />
                <div className="address-btns">
                  <DeleteIcon
                    onClick={() => handleDeleteLocation(location.id)}
                    fontSize="small"
                    color="primary"
                    className="delete-icon"
                  />
                </div>
                <hr />
              </div>
            )
          })}
        </CardContent>
      </Card>
    </div>
  )
}

export default LocationList
