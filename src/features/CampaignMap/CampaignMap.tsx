import React, { useState, useMemo, useEffect, useContext } from 'react'
import Map from 'components/Map'
import { states } from 'models/location'
import useLocations from 'hooks/useLocations'
import Loader from 'components/Loader/Loader'
import { UserContext } from 'providers/UserProvider'
import { getCompleteDate } from 'helpers/date.helper'
import useCampaignDates from 'hooks/useCampaignDates'
import DateRange from '@mui/icons-material/DateRange'
import CampaignMarkers from 'features/Markers/CampaignMarkers'
import { getGroupedCampaignDatesByCreatedDate } from 'helpers/helper'
import { campaignColors } from 'features/VolunteerDates/VolunteerDates'
import { Grid, Checkbox, FormGroup, FormControlLabel, Button } from '@mui/material'

import './CampaignMap.scss'
import type { State } from 'models/location'
import Icon from 'components/Icon/Icon'

export interface CampaignMapProps {
  isAdmin?: boolean
}

// TYPE GUARD
const isWalloniaState = (state: string): state is State =>
  ['Hainaut', 'Luxembourg', 'Liège', 'Namur', 'Walloon Brabant'].includes(state)

const CampaignMap: React.FC<CampaignMapProps> = ({ isAdmin }) => {
  const [selectedDates, setSelectedDates] = useState<string[]>()

  const { user } = useContext(UserContext)

  const { locations: allLocations, isLoading: isLoadingLocations, isFetching: isFetchingLocations } = useLocations()
  const { filteredCampaignDates, isLoading: isLoadingDates, isFetching: isFetchingDates } = useCampaignDates()

  const locations = useMemo(() => {
    if (isAdmin || !user?.provinces?.length) return allLocations

    return allLocations.filter(location => {
      // when there is no state, it's probably a city with the region name
      // if (!location.address?.state) return location
      if (
        location.address?.state &&
        isWalloniaState(location.address.state) &&
        user?.provinces?.includes(states[location.address.state])
      ) {
        return location
      }
    })
  }, [allLocations, isAdmin, user])

  const isLoading = isLoadingLocations || isFetchingLocations || isLoadingDates || isFetchingDates

  const handleChange = (checked: boolean, _date: string) => {
    if (!checked) {
      setSelectedDates(prev => prev?.filter(p => p !== _date))
    } else {
      setSelectedDates(prev => (prev ? [...prev, _date] : [_date]))
    }
  }

  useEffect(() => {
    if (!selectedDates && filteredCampaignDates?.[0]?.date)
      setSelectedDates([getCompleteDate(filteredCampaignDates[0].date)])
  }, [filteredCampaignDates, selectedDates])

  const campaignDatesgroupedByCreatedDate = useMemo(
    () => getGroupedCampaignDatesByCreatedDate(filteredCampaignDates),
    [filteredCampaignDates]
  )

  if (isLoading) return <Loader />

  return (
    <Grid container spacing={2} className="campaign-map-container">
      <Grid item lg={4} xs={12}>
        {!!selectedDates?.length ? (
          <Button onClick={() => setSelectedDates([])} variant="contained" className="unselect-btn">
            Désélectionner
          </Button>
        ) : Object.entries(campaignDatesgroupedByCreatedDate)?.length ? (
          <Button
            onClick={() => setSelectedDates(filteredCampaignDates.map(f => getCompleteDate(f.date)))}
            variant="contained"
            className="unselect-btn"
          >
            Tout sélectionner
          </Button>
        ) : (
          <div className="noTab">
            <Icon name="no_data" width={220} />
          </div>
        )}

        <FormGroup className="dates-group">
          {Object.entries(campaignDatesgroupedByCreatedDate).map(([, campaigns], i) => {
            const color = `${campaignColors[i % 10]}25`

            return campaigns?.map(({ date }) => {
              const _date = getCompleteDate(date)
              const _checked = selectedDates?.includes(_date) || false

              return (
                <FormControlLabel
                  key={_date}
                  checked={_checked}
                  onChange={(_event, checked) => handleChange(checked, _date)}
                  control={<Checkbox />}
                  className="dates-group-list"
                  label={
                    <div className="item">
                      <DateRange className="item-icon" />
                      {_date}
                    </div>
                  }
                  style={{ backgroundColor: color, borderRadius: '4px', margin: '3px' }}
                />
              )
            })
          })}
        </FormGroup>
      </Grid>

      <Grid item lg={8} xs={12}>
        <Map isAdmin={isAdmin} provinces={user?.provinces}>
          <CampaignMarkers locations={locations} selectedDates={selectedDates} />
        </Map>
      </Grid>
    </Grid>
  )
}

export default CampaignMap
