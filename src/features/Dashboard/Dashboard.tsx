import React, { type PropsWithChildren, useMemo, useContext } from 'react'
import { format } from 'date-fns'
import useUsers from 'hooks/useUsers'
import { ChosenTheme } from 'providers'
import useLocations from 'hooks/useLocations'
import Loader from 'components/Loader/Loader'
import { Grid, Typography } from '@mui/material'
import useRegistrations from 'hooks/useRegistrations'
import { getFormattedCurrency } from 'helpers/helper'
import type { Registration } from 'models/registration'
import { type AxisOptions, Chart, type UserSerie } from 'react-charts'

import './Dashboard.scss'
import Card from 'components/Card'

interface ChartData {
  label: string
  value: number
}

interface ChartModel {
  label: string
  data: ChartData[]
}

export interface DashboardProps extends PropsWithChildren {
  isAdmin?: boolean
}

const Dashboard: React.FC<DashboardProps> = ({ isAdmin = false }) => {
  const { theme } = useContext(ChosenTheme)
  const { locations } = useLocations()
  const { users, isLoading: isLoadingUsers } = useUsers()
  const { registrations, isLoading: isLoadingRegistrations } = useRegistrations()

  const registrationsThisYear: UserSerie<ChartData>[] | undefined = useMemo(() => {
    if (!registrations?.length) return

    const thisYear = new Date().getFullYear()
    const filteredRegistrations = registrations.filter(r => new Date(r.date).getFullYear() === thisYear)
    const sortedRegistrationsByUserId = filteredRegistrations.reduce<Record<Registration['userId'], Registration[]>>(
      (acc, curr) => {
        if (acc[curr.userId]) acc[curr.userId].push(curr)
        else acc[curr.userId] = [curr]
        return acc
      },
      {}
    )

    return Object.entries(sortedRegistrationsByUserId).map<ChartModel>(([userId, data]) => {
      const user = users?.find(u => u.id === userId)
      return {
        label: `${user?.firstName} ${user?.lastName}` || user?.email || userId,
        data: data.map(d => ({ label: format(new Date(d.date), 'dd MMM yyyy'), value: d.amount || 0 }))
      }
    })
  }, [registrations, users])

  const totalAmountPerUser: UserSerie<ChartData>[] | undefined = useMemo(() => {
    if (!registrationsThisYear?.length) return

    const thisYear = new Date().getFullYear()
    return registrationsThisYear.map<ChartModel>(r => ({
      label: r.label!,
      data: [
        {
          label: thisYear.toString(),
          value: r.data.reduce((acc, curr) => (acc = acc += curr.value), 0)
        }
      ]
    }))
  }, [registrationsThisYear])

  const bestAmountPerLocation: UserSerie<ChartData>[] | undefined = useMemo(() => {
    if (!registrations?.length) return

    const filteredByLocations: Record<string, number> = registrations.reduce<Record<string, number>>((acc, curr) => {
      if (!curr.locationId || !curr.amount) return acc
      acc[curr.locationId] = acc[curr.locationId] ? (acc[curr.locationId] += curr.amount) : curr.amount
      return acc
    }, {})

    return Object.entries(filteredByLocations)
      .sort((a, b) => b[1] - a[1])
      .map<ChartModel>(([locationId, amount]) => {
        const location = locations?.find(u => u.id === locationId)
        return {
          label: location?.address_name || '',
          data: [{ value: amount, label: location?.name || '' }]
        }
      })
  }, [registrations, locations])

  const bestAmountPerLocationPerUser: UserSerie<ChartData>[] | undefined = useMemo(() => {
    if (!registrations?.length) return

    const filteredByUser = registrations.reduce<
      Record<string, { amount: number; locationId: Registration['locationId'] }>
    >((acc, curr) => {
      if (!curr.userId || !curr.amount) return acc
      acc[curr.userId] =
        acc[curr.userId]?.amount > curr.amount
          ? acc[curr.userId]
          : {
              amount: curr?.amount,
              locationId: curr.locationId
            }
      return acc
    }, {})

    return Object.entries(filteredByUser)
      .sort((a, b) => b[1].amount - a[1].amount)
      .map<ChartModel>(([userId, { amount, locationId }]) => {
        const user = users.find(u => u.id === userId)
        const location = locations?.find(u => u.id === locationId)
        return {
          label: `${user?.firstName} ${user?.lastName} (${user?.email})`,
          data: [{ value: amount, label: location?.name || '' }]
        }
      })
  }, [locations, registrations, users])

  const totalAnnualAmount = useMemo(() => {
    if (!totalAmountPerUser) return undefined

    const annualTotal = totalAmountPerUser.reduce<number>(
      (total, current) => (total = total + current.data.reduce((acc, curr) => (acc = acc + curr.value), 0)),
      0
    )
    return getFormattedCurrency(annualTotal)
  }, [totalAmountPerUser])

  const primaryAxis = React.useMemo(
    (): AxisOptions<ChartData> => ({
      getValue: data => data.label
    }),
    []
  )

  const secondaryAxes = React.useMemo(
    (): AxisOptions<ChartData>[] => [
      {
        getValue: data => data.value,
        scaleType: 'linear',
        min: 0,
        elementType: 'bar'
      }
    ],
    []
  )

  if (isLoadingRegistrations || isLoadingUsers) return <Loader />

  return (
    <Grid container className="dashboard">
      <Grid item sm={6} xs={12} className="dashboard-item">
        <Typography variant="h5" color="text.secondary" gutterBottom>
          Ventes/Année
        </Typography>
        {registrationsThisYear && (
          <Chart
            options={{
              data: registrationsThisYear,
              primaryAxis,
              secondaryAxes,
              dark: theme === 'dark'
            }}
          />
        )}
      </Grid>

      <Grid item sm={6} xs={12} className="dashboard-item">
        <Typography variant="h5" color="text.secondary" gutterBottom>
          Ventes/Lieu
        </Typography>
        {bestAmountPerLocation && (
          <Chart
            options={{
              data: bestAmountPerLocation,
              primaryAxis,
              secondaryAxes,
              dark: theme === 'dark'
            }}
          />
        )}
      </Grid>

      <Grid item sm={6} xs={12} className="dashboard-item">
        <Typography variant="h5" color="text.secondary" gutterBottom>
          Meilleure Vente/Bénévole
        </Typography>
        {bestAmountPerLocationPerUser && (
          <Chart
            options={{
              data: bestAmountPerLocationPerUser,
              primaryAxis,
              secondaryAxes,
              dark: theme === 'dark'
            }}
          />
        )}
      </Grid>

      {isAdmin && (
        <Grid item sm={6} xs={12} className="dashboard-item">
          <Typography variant="h5" color="text.secondary" gutterBottom>
            Ventes/Bénévole
          </Typography>
          {totalAmountPerUser && (
            <Chart
              options={{
                data: totalAmountPerUser,
                primaryAxis,
                secondaryAxes,
                dark: theme === 'dark'
              }}
            />
          )}
        </Grid>
      )}

      <Grid item xs={12}>
        <Card className="dashboard-card">Total annuel: {totalAnnualAmount}</Card>
      </Grid>
    </Grid>
  )
}

export default Dashboard
