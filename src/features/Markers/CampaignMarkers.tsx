import React, { useMemo } from 'react'
import { Icon } from 'leaflet'
import _uniqBy from 'lodash/uniqBy'
import useUsers from 'hooks/useUsers'
import _flatten from 'lodash/flatten'
import { Marker, Popup } from 'react-leaflet'
import type { Location } from 'models/location'
import useRegistrations from 'hooks/useRegistrations'
import { getCompleteDate } from 'helpers/date.helper'
import type { Registration } from 'models/registration'

import redpin from 'assets/icons/mark_red.svg'
import greenpin from 'assets/icons/mark_green.svg'

// Creating new icon for leaflet
const RedIcon = new Icon({ iconUrl: redpin })
const GreenIcon = new Icon({ iconUrl: greenpin })

export interface CampaignMarkersProps {
  locations: Location[]
  selectedDates?: string[]
}

const CampaignMarkers: React.FC<CampaignMarkersProps> = ({ locations, selectedDates = [] }) => {
  const { registrations } = useRegistrations()
  const { users } = useUsers()

  const markerList = useMemo(() => {
    if (!selectedDates?.length) return {}

    return registrations.reduce<Record<string, Registration[]>>((acc, curr) => {
      if (selectedDates.includes(getCompleteDate(curr.date))) {
        acc[curr.date] = acc[curr.date] ? [...acc[curr.date], curr] : [curr]
      }
      return acc
    }, {})
  }, [registrations, selectedDates])

  return (
    <>
      {locations.map(location => {
        if (!location.coordinates) return

        const _filteredRegistrationsByDates = _flatten(Object.values(markerList))
        const _hasMinimumOneVolunteer = _filteredRegistrationsByDates
          ?.map(r => r.locationId === location.id)
          .some(Boolean)
        const _filteredUsers = _uniqBy(_filteredRegistrationsByDates, 'userId').map(r =>
          users.find(u => u.id === r.userId)
        )

        return (
          <Marker
            key={location.id}
            position={[location.coordinates.lat, location.coordinates.lon]}
            icon={_hasMinimumOneVolunteer ? GreenIcon : RedIcon}
          >
            <>
              <Popup>
                <h2>{location.name}</h2>
                <b>Adresse</b> : {location.address_name}
                {_hasMinimumOneVolunteer && (
                  <div>
                    <b>Bénévoles</b>
                    {': '}
                    {_hasMinimumOneVolunteer ? _filteredUsers.length : 0}
                    <br></br>
                    <b>Email(s)</b>
                    {': '}
                    {_filteredUsers.map(u => u?.email).join(', ')}
                  </div>
                )}
              </Popup>
            </>
          </Marker>
        )
      })}
    </>
  )
}

export default CampaignMarkers
