import React, { useState } from 'react'
import { Icon } from 'leaflet'
import { Marker, Popup } from 'react-leaflet'
import EditIcon from '@mui/icons-material/Edit'
import type { Location } from 'models/location'
import LocationForm from 'features/LocationForm'
import type { UseMutationResult } from 'react-query'

import bluepin from 'assets/icons/mark_blue.svg'

// Creating new icon for leaflet
const BlueIcon = new Icon({ iconUrl: bluepin })

export interface LocationMarkersProps {
  locations?: Location[]
  edit: UseMutationResult<void, unknown, Location, unknown>
}

const LocationMarkers: React.FC<LocationMarkersProps> = ({ locations, edit }) => {
  const [isOpenLocationModal, setIsOpenLocationModal] = useState(false)

  const handleSubmit = (location: Location | Omit<Location, 'id'>) => {
    'id' in location && location?.id && edit.mutate(location)
    setIsOpenLocationModal(false)
  }

  return (
    <>
      {locations?.map(location => {
        const { id, name, address_name, coordinates } = location
        if (!coordinates) return
        return (
          <Marker key={id} position={[coordinates.lat, coordinates.lon]} icon={BlueIcon}>
            <>
              <LocationForm
                location={location}
                isOpen={isOpenLocationModal}
                onClose={() => setIsOpenLocationModal(false)}
                onSubmit={handleSubmit}
              />
              <Popup>
                <h2>
                  {name}&nbsp;
                  <EditIcon
                    onClick={() => setIsOpenLocationModal(true)}
                    fontSize="small"
                    color="primary"
                    className="map-edit-icon"
                  />
                </h2>
                <b>Adresse</b> : {address_name}
              </Popup>
            </>
          </Marker>
        )
      })}
    </>
  )
}

export default LocationMarkers
