import React, { useContext, useState } from 'react'
import Card from 'components/Card'
import { ChosenTheme } from 'providers'
import DatePicker from 'react-datepicker'
import { Icon } from 'components/Icon/Icon'
import Loader from 'components/Loader/Loader'
import type { CampaignDate } from 'models/campaign'
import { addDays, differenceInDays } from 'date-fns'
import { getCompleteDate } from 'helpers/date.helper'
import useCampaignDates from 'hooks/useCampaignDates'
import { Button, Grid, Typography } from '@mui/material'
import DateRangeIcon from '@mui/icons-material/DateRange'
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline'

import './DatesForm.scss'

const DateForm: React.FC = () => {
  const { theme } = useContext(ChosenTheme)
  const { campaignDates, status, addCampaignDate, removeCampaignDate } = useCampaignDates()

  const [startDate, setStartDate] = useState<Date>()
  const [endDate, setEndDate] = useState<Date>()
  const [newDates, setNewDates] = useState<Date[]>([])

  const getDaysArray = (date1: Date, date2: Date): Date[] => {
    let date = date1
    const _dates = []

    while (date <= date2) {
      _dates.push(date)
      date = addDays(date, 1)
    }

    return _dates
  }

  const handleClickOnCancel = () => {
    setStartDate(undefined)
    setEndDate(undefined)
    setNewDates([])
  }

  const handleOnSave = () => {
    const today = new Date()
    newDates.map(date => {
      return addCampaignDate.mutate({
        year: new Date(date).getFullYear().toString(),
        date: date.toISOString(),
        createdAt: today.toISOString()
      })
    })
    handleClickOnCancel()
  }

  const handleChangeDatesSelection = (date: Date, dateType: 'startDate' | 'endDate') => {
    if (dateType === 'startDate') setStartDate(date)
    if (dateType === 'endDate') setEndDate(date)
    if (dateType === 'endDate' && startDate) setNewDates(getDaysArray(startDate, date))
  }

  const renderCampainCard = (data: Date[] | CampaignDate[] | undefined, title: string) => {
    if (!data?.length) return

    const _dates: { date: string; id?: string }[] = data.map(d => {
      return 'id' in d ? { date: d.date, id: d.id } : { date: d.toISOString(), id: undefined }
    })

    return (
      <Card className="dates-form-list__card">
        <Typography variant="h5" gutterBottom>
          {title}:
        </Typography>
        <ul>
          {_dates.map(({ date, id }, i) => (
            <li
              key={`${date}-${i}`}
              className={`dates-form-list__card-item ${i !== 0 && differenceInDays(new Date(date), new Date(_dates[i - 1]?.date)) > 1 && `dates-form-list__card__item-underline-${theme}`}`}
            >
              <DateRangeIcon />
              &nbsp;
              {getCompleteDate(date)}
              &nbsp;
              {id && !removeCampaignDate.isLoading && (
                <DeleteOutlineIcon
                  onClick={() => removeCampaignDate.mutate(id)}
                  color="primary"
                  fontSize="small"
                  className="dates-form-list-delete-icon"
                />
              )}
              {removeCampaignDate.isLoading && <Loader size="10" />}
            </li>
          ))}
        </ul>
      </Card>
    )
  }

  return (
    <Grid container spacing={4} className="dates-form">
      <Grid item sm={6} xs={12}>
        <span className="dates-form-wrapper">
          <Typography sx={{ fontSize: 18 }} color="text.secondary" gutterBottom>
            Date de début:
          </Typography>
          <DatePicker
            locale="fr"
            dateFormat="dd/MM/yyyy"
            selected={startDate}
            onChange={(date: Date) => handleChangeDatesSelection(date, 'startDate')}
            selectsStart
            startDate={startDate}
            endDate={endDate}
            maxDate={endDate}
            placeholderText="Date de début"
          />
        </span>

        <span className="dates-form-wrapper">
          <Typography sx={{ fontSize: 18 }} color="text.secondary" gutterBottom>
            Date de fin:
          </Typography>
          <DatePicker
            locale="fr"
            dateFormat="dd/MM/yyyy"
            selected={endDate}
            onChange={(date: Date) => handleChangeDatesSelection(date, 'endDate')}
            selectsEnd
            startDate={startDate}
            endDate={endDate}
            minDate={startDate}
            placeholderText="Date de fin"
          />
        </span>

        <div className="dates-form-btns">
          <Button onClick={handleOnSave} variant="contained" disabled={!newDates || addCampaignDate.isLoading}>
            Sauvegarder
          </Button>
          <Button onClick={handleClickOnCancel} variant="outlined">
            Annuler
          </Button>
        </div>
      </Grid>

      <Grid item sm={6} xs={12} className="dates-form-list">
        {status === 'error' && <p>Error fetching data</p>}
        {status === 'loading' && <Loader />}
        {status === 'success' && (newDates.length || campaignDates?.length) ? (
          <>
            {renderCampainCard(newDates, 'Nouvelle(s) campagne(s)')}
            {renderCampainCard(campaignDates, 'Campagnes existantes')}
          </>
        ) : (
          <div className="dates-form-img">
            <Icon name="reminder" alt="Pas de dates" width={250} />
          </div>
        )}
      </Grid>
    </Grid>
  )
}

export default DateForm
