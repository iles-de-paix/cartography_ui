import React, { useState } from 'react'
import Card from 'components/Card'
import classNames from 'classnames'
import Modal from 'components/Modal'
import { Icon } from 'components/Icon/Icon'
import useLocations from 'hooks/useLocations'
import Loader from 'components/Loader/Loader'
import { Grid, Typography } from '@mui/material'
import PlaceIcon from '@mui/icons-material/Place'
import useRegistrations from 'hooks/useRegistrations'
import { getCompleteDate } from 'helpers/date.helper'
import type { Registration } from 'models/registration'
import RegistrationForm from 'features/RegistrationForm'
import { getFormattedCurrency, getRangeHours } from 'helpers/helper'

import './History.scss'

const History: React.FC = () => {
  const { filteredPastRegistrationsByUser, isLoading, isFetching } = useRegistrations()
  const { locations, isLoading: isLoadingLocations, isFetching: isFetchingLocation } = useLocations()

  const [isOpen, setIsOpen] = useState(false)
  const [selectedRegistration, setSelectedRegistration] = useState<Registration>()

  const handleClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>, registration: Registration) => {
    e.preventDefault()
    setSelectedRegistration(registration)
    setIsOpen(i => !i)
  }

  if (isLoading || isFetching || isLoadingLocations || isFetchingLocation) return <Loader />

  return (
    <Grid container className="history-wrapper" direction="column">
      {!!filteredPastRegistrationsByUser?.length ? (
        <Grid item>
          <Modal open={isOpen} onClose={() => setIsOpen(false)}>
            <RegistrationForm registration={selectedRegistration} onClose={() => setIsOpen(false)} />
          </Modal>

          <Typography variant="h5" color="text.secondary" gutterBottom>
            Mon historique:
          </Typography>

          <div className="history-list">
            {filteredPastRegistrationsByUser.map(registration => {
              const location = locations?.find(l => l.id === registration.locationId)
              return (
                <Card key={registration.id} className="history-list-card" onClick={e => handleClick(e, registration)}>
                  <div className="history-list-card__header">
                    <Typography variant="h6" color="text.secondary" gutterBottom>
                      {`${getCompleteDate(registration.date)} (${getRangeHours(
                        registration.morningSelected,
                        registration.afternoonSelected
                      )})`}
                    </Typography>
                    <div
                      className={classNames('history-wrapper-card__header__amount', {
                        'red-amount': !registration.amount
                      })}
                    >
                      {getFormattedCurrency(registration.amount)}
                    </div>
                  </div>
                  {location && (
                    <div className="history-wrapper-card__address">
                      <PlaceIcon color="primary" />
                      {` ${location.name} (${location.address_name})`}
                    </div>
                  )}
                </Card>
              )
            })}
          </div>
        </Grid>
      ) : (
        <div className="no-history">
          <Typography variant="h5" color="text.secondary" gutterBottom>
            Aucun historique
          </Typography>
          <Icon name="no_data" alt="[VIDE]" className="no-tab-img" width={220} />
        </div>
      )}
      <Icon name="history" width={145} className="history-img" />
    </Grid>
  )
}

export default History
