import React, { useMemo, useState } from 'react'
import AddIcon from '@mui/icons-material/Add'
import useLocations from 'hooks/useLocations'
import LocationForm from 'features/LocationForm'
import useRegistrations from 'hooks/useRegistrations'
import { useForm, type SubmitHandler, Controller, useWatch } from 'react-hook-form'

import {
  Grid,
  Button,
  Checkbox,
  TextField,
  FormControl,
  Autocomplete,
  FormHelperText,
  FormControlLabel
} from '@mui/material'
import Snackbar from 'components/Snackbar'
import { Icon } from 'components/Icon/Icon'
import SaveIcon from '@mui/icons-material/Save'

import type { Location } from 'models/location'
import type { Registration } from 'models/registration'

import './VolunteerLocationForm.scss'

export interface VolunteerLocationFormProps {
  registration: Registration
}

const VolunteerLocationForm: React.FC<VolunteerLocationFormProps> = ({ registration }) => {
  const { locations, addLocation } = useLocations()
  const { editRegistration } = useRegistrations()

  const {
    watch,
    control,
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<Registration>({
    defaultValues: { ...registration }
  })
  const [isOpen, setIsOpen] = useState(false)

  const watchedLocationIdField = useWatch({
    control,
    name: 'locationId'
  })

  const selectedLocation = useMemo(() => {
    const _locationId = watchedLocationIdField || registration?.locationId
    if (!locations?.length || !_locationId) return

    const _location = locations.find(l => l.id === _locationId)
    if (!_location) return

    return {
      label: `${_location.name} (${_location.address_name})`,
      key: _location.id
    }
  }, [locations, registration, watchedLocationIdField])

  const handSubmitForm: SubmitHandler<Registration> = (data: Registration) => {
    editRegistration.mutate(data)
  }

  const handleSubmitNewLocation = (location: Omit<Location, 'id'> & { id?: string }) => {
    addLocation.mutate(location)
    setIsOpen(false)
  }

  return (
    <form onSubmit={handleSubmit(handSubmitForm)} className="volunteer-form" key={crypto.randomUUID()}>
      <Grid className="grid" container spacing={1}>
        <Grid item sm={8} xs={12}>
          <FormControl error={!!errors?.locationId} fullWidth>
            <Controller
              name="locationId"
              control={control}
              defaultValue=""
              render={props => (
                <Autocomplete
                  disablePortal
                  autoHighlight
                  noOptionsText="Aucune adresse"
                  onChange={(_event, option) => props.field.onChange(option?.key)}
                  getOptionLabel={option => option.label}
                  getOptionKey={option => option.key}
                  isOptionEqualToValue={(option, value) => option.key === value.key}
                  defaultValue={selectedLocation ?? undefined}
                  options={
                    !!locations.length
                      ? locations.map(l => ({ label: `${l.name} (${l.address_name})`, key: l.id }))
                      : selectedLocation
                        ? [selectedLocation]
                        : []
                  }
                  renderInput={params => (
                    <TextField
                      {...params}
                      required
                      label="Lieu"
                      InputLabelProps={{
                        shrink: true
                      }}
                      inputRef={() => register('locationId', { required: true })}
                    />
                  )}
                />
              )}
            />
            {errors?.locationId && (
              <FormHelperText id="locationId-error" error>
                Le lieu est obligatoire
              </FormHelperText>
            )}
          </FormControl>

          <LocationForm isOpen={isOpen} onClose={() => setIsOpen(false)} onSubmit={handleSubmitNewLocation} />
          <Button onClick={() => setIsOpen(true)} variant="contained" className="add-location-btn">
            <AddIcon />
            &nbsp; Ajouter un lieu
          </Button>
        </Grid>

        <Grid item sm={4} xs={12} className="container-img">
          <Icon name="reminder" height={150} className="reminder-img" />
        </Grid>

        <Grid item xs={12} className="section">
          <FormControlLabel
            label="Matin"
            control={<Checkbox checked={watch('morningSelected')} />}
            {...register('morningSelected')}
          />

          <FormControlLabel
            label="Après-midi"
            control={<Checkbox checked={watch('afternoonSelected')} />}
            {...register('afternoonSelected')}
          />
        </Grid>

        <Grid item sm={6} xs={12}>
          <Button
            type="submit"
            variant="contained"
            className="saveButton"
            disabled={editRegistration.status === 'loading'}
          >
            <SaveIcon />
            &nbsp;Enregistrer
          </Button>
        </Grid>
      </Grid>
      {editRegistration.status === 'error' && <Snackbar isOpen message="Enregistrement échoué" />}
      {editRegistration.status === 'success' && <Snackbar status="success" isOpen message="Mis à jour" />}
    </form>
  )
}

export default VolunteerLocationForm
