import { initReactI18next } from 'react-i18next'
import { type ResourceLanguage, use } from 'i18next'

// translations
import french from 'locales/fr-fr.json'
import english from 'locales/en-us.json'

export const languages = ['fr-FR', 'en-US', 'test'] as const
const languageNames = ['french', 'english', 'localizationDebug'] as const

export type Language = (typeof languages)[number]
export type LanguageName = (typeof languageNames)[number]
export type Locale = typeof french

export const datePickerLanguage: Record<Language, string> = {
  'en-US': 'en',
  'fr-FR': 'fr',
  test: 'zh'
}

// const _localizationDebug = localeStringsOverride(english as unknown) ?? english

export const resources: Record<Language, ResourceLanguage> = {
  'fr-FR': { translation: french },
  'en-US': { translation: english },
  test: { translation: english } // _localizationDebug }
}

use(initReactI18next).init({
  resources,
  lng: 'fr-FR',
  interpolation: {
    escapeValue: false // react already safes from xss => https://www.i18next.com/translation-function/interpolation#unescape
  },
  fallbackLng: 'fr-FR'
})
