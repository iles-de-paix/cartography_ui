import React, { useContext } from 'react'
import { ChosenTheme } from 'providers'
import DarkModeToggle from './DarkModeToggle'
import LogoutIcon from '@mui/icons-material/Logout'
import { Link, useNavigate } from 'react-router-dom'
import { AppBar, Checkbox, Toolbar } from '@mui/material'
import { UserContext, localStorageKey } from 'providers/UserProvider'

import ilesDePaix from 'assets/iles-de-paix-default-logo.png'

import './Header.scss'

const Header: React.FC = () => {
  const { theme } = useContext(ChosenTheme)
  const navigate = useNavigate()
  const { user, setUser, setDemoMode } = useContext(UserContext)

  const logout = () => {
    setUser(undefined)
    navigate('/', { replace: true })
    localStorage.removeItem(localStorageKey)
  }

  return (
    <AppBar position="fixed">
      <Toolbar variant="dense" className="toolbar">
        <Link to="/">
          <img
            src={ilesDePaix}
            alt="Logo iles de paix"
            title="Page d'acceuil"
            className={`logo-iles-de-paix logo-iles-de-paix-${theme}`}
            width={70}
          />
        </Link>
        {user && (
          <div className="logoutIcon" onClick={logout} title="Se déconnecter">
            <LogoutIcon />
          </div>
        )}
        {/* // TODO: remove this part for prod perspective */}
        <div>
          <label htmlFor="demo-mode">Demo: </label>
          <Checkbox
            id="demo-mode"
            color="primary"
            className="demoMode"
            defaultChecked={true}
            onChange={({ target: { checked } }) => {
              setDemoMode(!!checked)
            }}
          />
        </div>
        <DarkModeToggle title="Changer de thème" />
      </Toolbar>
    </AppBar>
  )
}

export default Header
