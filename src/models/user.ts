import type { Province } from './location'

export type Role = 'volunteer' | 'manager' | 'admin'
export enum ERole {
  'volunteer' = 0,
  'manager' = 1,
  'admin' = 2
}
type RoleKey = keyof typeof ERole

export interface User {
  id: string
  firstName?: string
  lastName?: string
  email: string
  phoneNumber?: string
  role: (typeof ERole)[RoleKey]
  provinces?: Province[]
}
