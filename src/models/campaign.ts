export interface CampaignDate {
  date: string
  id: string
  createdAt: string
  year: string
}
