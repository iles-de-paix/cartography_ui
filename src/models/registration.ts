export interface Registration {
  id: string
  amount?: number
  afternoonSelected?: boolean
  date: string
  locationId?: string
  morningSelected?: boolean
  userId: string
}
