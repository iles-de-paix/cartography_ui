export interface Address {
  'ISO3166-2-lvl4'?: string
  'ISO3166-2-lvl6'?: string
  country_code?: string
  country: string
  county?: string
  municipality?: string
  region?: string
  state?: string
  village?: string
}

export interface Location {
  id: string
  name?: string
  address_name: string
  address: Address
  place_id?: number
  osm_id?: number
  coordinates?: {
    lat: number
    lon: number
  }
  createdAt?: string
  modifiedAt?: string
}

export interface StreetMapLocation {
  address: Address
  addresstype: string
  boundingbox: string[]
  category: string
  display_name: string
  importance: number
  lat: string
  licence: string
  lon: string
  name: string
  osm_id: number
  osm_type: string
  place_id: number
  place_rank: number
  type: string
}

export interface GeoPoint2d {
  lon: number
  lat: number
}

export type Coordinate = [number, number]
export type Coordinates = Coordinate[][] | Coordinate[][][]
export interface Geometry {
  coordinates: Coordinates
  type: string
}

export interface GeoShape {
  type: string
  geometry: Geometry
  properties: {
    Name?: string
    NSI?: number
  }
}

export interface GeoJson {
  geo_point_2d: GeoPoint2d
  geo_shape: GeoShape
  year: string
  reg_code: string[]
  prov_code: string[]
  prov_area_code: string
  prov_type: string
  prov_name_fr: string[]
  prov_name_lower_fr: string
  prov_name_upper_fr: string
  prov_name_nl: string[]
  prov_name_upper_nl: string
  prov_name_lower_nl: string
  prov_name_de: string[]
  prov_name_lower_de: string
  prov_name_upper_de: string
  reg_name_de: string[]
  reg_name_nl: string[]
  reg_name_fr: string[]
}

export const provinces = ['liege', 'namur', 'brabantWallon', 'hainaut', 'luxembourg', 'bruxelles'] as const
export type Province = (typeof provinces)[number]

export const walloniaStates = ['Hainaut', 'Luxembourg', 'Liège', 'Namur', 'Walloon Brabant'] as const
export type State = (typeof walloniaStates)[number]
export const states: Record<State, Province> = {
  Hainaut: 'hainaut',
  Luxembourg: 'luxembourg',
  Liège: 'liege',
  Namur: 'namur',
  'Walloon Brabant': 'brabantWallon'
}
