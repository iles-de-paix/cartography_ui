import React, { type ComponentProps, useContext } from 'react'
import classNames from 'classnames'
import { ChosenTheme } from 'providers'
import { Card as CardMUI } from '@mui/material'

import './Card.scss'

interface TabListProps extends ComponentProps<typeof CardMUI> {
  className?: string
  children: React.ReactNode
  scale?: boolean
}

const Card: React.FC<TabListProps> = ({ className, children, scale = false, ...props }) => {
  const { theme } = useContext(ChosenTheme)

  return (
    <CardMUI {...props} className={classNames({ [`scale-card-${theme}`]: scale }, `card-${theme}`, className)}>
      {children}
    </CardMUI>
  )
}
export default Card
