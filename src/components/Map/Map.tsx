import React, { type PropsWithChildren, useState } from 'react'
import Card from 'components/Card'
import { Switch } from '@mui/material'
import DrawProvinces from 'components/DrawProvinces'
import { MapContainer, TileLayer } from 'react-leaflet'

import type { Province } from 'models/location'
import type { LatLngExpression } from 'leaflet'

import './Map.scss'

export interface MapProps extends PropsWithChildren {
  center?: LatLngExpression
  zoom?: number
  isAdmin?: boolean
  provinces?: Province[]
}

const Map: React.FC<MapProps> = ({ children, center = [50.6112, 4.1012], zoom = 8, provinces }) => {
  const [displayLayers, setDisplayLayers] = useState(true)

  return (
    <Card className="map-container">
      <span className="map-container-layers">
        Afficher les calques: <Switch checked={displayLayers} onChange={() => setDisplayLayers(l => !l)} />
      </span>
      <MapContainer center={center} fadeAnimation zoom={zoom} scrollWheelZoom={true} className="map-element">
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {displayLayers && <DrawProvinces provinces={provinces} />}
        {children}
      </MapContainer>
    </Card>
  )
}

export default Map
