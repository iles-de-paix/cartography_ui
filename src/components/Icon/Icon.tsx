import React, { type ImgHTMLAttributes, useContext } from 'react'
import { ChosenTheme } from 'providers'

// import Calendar from 'assets/icons/Calendar'

import calendar from 'assets/icons/calendar.svg'
import history from 'assets/icons/history.svg'
import location from 'assets/icons/location.svg'
import manager from 'assets/icons/manager.svg'
import no_data from 'assets/icons/no_data.svg'
import not_found from 'assets/icons/not_found.svg'
import reminder from 'assets/icons/reminder.svg'
import sales from 'assets/icons/sales.svg'
import volunteer_1 from 'assets/icons/volunteer_1.svg'
import volunteer_2 from 'assets/icons/volunteer_2.svg'
import volunteer_3 from 'assets/icons/volunteer_3.svg'

export type IconName =
  | 'calendar'
  | 'history'
  | 'location'
  | 'manager'
  | 'no_data'
  | 'not_found'
  | 'reminder'
  | 'sales'
  | 'volunteer_1'
  | 'volunteer_2'
  | 'volunteer_3'

export type Color = 'primary' | 'secondary' | 'ternary' | 'fourth' | 'fifth' | 'globalTheme'

// light theme
const lightTheme: Record<Color, string> = {
  primary: '#f9a826', // orange
  secondary: '#e6e6e6', // light grey
  ternary: '#ffb8b8', // skin
  fourth: '#3f3d56', // light dark blue
  fifth: '#2f2e41', // dark blue
  globalTheme: '#fff' // light
}

// dark theme
const darkTheme: Record<Color, string> = {
  primary: '#1b4f14', // green
  secondary: '#e1e3e1', // light grey
  ternary: '#cccccc', // light dark grey
  fourth: '#ff9100', // light dark orange
  fifth: '#965602', // dark orange
  globalTheme: '#000000' // dark
}

// Object.entries(lightTheme).forEach(([key, color]) => {
//   test = test.replace(color, darkTheme[key as Colors]);
// });

// IMPLEMENTATION:
// <Icons iconName="search" theme={theme} />

export interface IconsProps extends Partial<ImgHTMLAttributes<HTMLImageElement>> {
  name: IconName
}

export const Icon: React.FC<IconsProps> = ({ name, ...props }) => {
  const { theme } = useContext(ChosenTheme)
  // TODO: implement dynamic colors in svg files ?! -> const color =
  theme === 'light' ? lightTheme : darkTheme

  switch (name) {
    case 'calendar':
      return <img src={calendar} alt={name} {...props} />
    case 'history':
      return <img src={history} alt={name} {...props} />
    case 'manager':
      return <img src={manager} alt={name} {...props} />
    case 'location':
      return <img src={location} alt={name} {...props} />
    case 'no_data':
      return <img src={no_data} alt={name} {...props} />
    case 'not_found':
      return <img src={not_found} alt={name} {...props} />
    case 'reminder':
      return <img src={reminder} alt={name} {...props} />
    case 'sales':
      return <img src={sales} alt={name} {...props} />
    case 'volunteer_1':
      return <img src={volunteer_1} alt={name} {...props} />
    case 'volunteer_2':
      return <img src={volunteer_2} alt={name} {...props} />
    case 'volunteer_3':
      return <img src={volunteer_3} alt={name} {...props} />
    default:
      const exhaustiveCheck: never = name
      throw new Error(`Unhandled color case: ${exhaustiveCheck}`)
  }
}

export default Icon
