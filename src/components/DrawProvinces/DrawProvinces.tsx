import React, { useMemo } from 'react'
import json from 'assets/provinces'
import { Polygon } from 'react-leaflet'

import type { LatLngExpression } from 'leaflet'
import type { GeoJson, Province, Coordinates, Coordinate } from 'models/location'

const colors = ['#16537e', '#f6b26b', '#f44336', '#6aa84f', '#e6e6fa', '#ffb8b8'] as const
export type GeoObject = { json: GeoJson[]; color: (typeof colors)[number] | string }
export type ProvinceJson = Record<Province, GeoObject>
export const provinceGeoJson: ProvinceJson = {
  brabantWallon: { json: json.BrabantWallon as unknown as GeoJson[], color: '#16537e' },
  bruxelles: { json: json.Bruxelles as unknown as GeoJson[], color: '#f6b26b' },
  hainaut: { json: json.Hainaut as unknown as GeoJson[], color: '#f44336' },
  liege: { json: json.Liege as unknown as GeoJson[], color: '#6aa84f' },
  luxembourg: { json: json.Luxembourg as unknown as GeoJson[], color: '#e6e6fa' },
  namur: { json: json.Namur as unknown as GeoJson[], color: '#ffb8b8' }
}

export interface DrawProvincesProps {
  provinces?: Province[]
}

const isNestedArray = (coordinates: Coordinates): coordinates is Coordinate[][][] => {
  return coordinates[0].every(element => element.length !== 2)
}

const DrawProvinces: React.FC<DrawProvincesProps> = ({ provinces }) => {
  const _geoObject: GeoObject = useMemo(() => {
    if (!provinces?.length) return { json: json.Wallonie as unknown as GeoJson[], color: 'lime' }

    const _json = provinces.reduce<GeoJson[]>((acc, province) => acc.concat(provinceGeoJson[province].json), [])
    return { json: _json, color: provinceGeoJson[provinces[0]].color }
  }, [provinces])

  return (
    <>
      {_geoObject.json.map((data, i) => {
        const _array = data.geo_shape.geometry.coordinates
        const _coordinates: Coordinate[] = isNestedArray(_array)
          ? _array.reduce<Coordinate[]>((acc, curr) => acc.concat(curr.flat()), [])
          : _array[0]
        const coordinates: LatLngExpression[] = _coordinates.map(c => [c[1], c[0]])

        return (
          <Polygon
            key={i}
            pathOptions={{ fillColor: _geoObject.color, fillOpacity: 0.1, color: 'yellow' }}
            positions={coordinates}
          />
        )
      })}
    </>
  )
}

export default DrawProvinces
