import React, { type PropsWithChildren } from 'react'
import { Drawer } from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'

import './Slideout.scss'

export interface SlideoutProps extends PropsWithChildren {
  className?: string
  isOpen: boolean
  position?: 'bottom' | 'left' | 'right' | 'top'
  width?: 'small' | 'medium' | 'large'
  onClose: () => void
}

const Slideout: React.FC<SlideoutProps> = ({
  className,
  isOpen,
  children,
  position = 'right',
  width = 'large',
  onClose,
  ...props
}) => {
  return (
    <Drawer anchor={position} open={isOpen} onClose={onClose} {...props}>
      <div className={`drawer-wrapper-container__${width} ${className}`}>
        <div className="drawer-wrapper-content">
          <CloseIcon className="close-icon" onClick={onClose} />
          {children}
        </div>
      </div>
    </Drawer>
  )
}

export default Slideout
