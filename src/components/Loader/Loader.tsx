import React, { type ComponentProps } from 'react'
import { CircularProgress } from '@mui/material'

import './Loader.scss'

const Loader: React.FC<ComponentProps<typeof CircularProgress>> = ({ ...props }) => {
  return (
    <div className="loaderWrapper">
      <CircularProgress {...props} />
    </div>
  )
}

export default Loader
