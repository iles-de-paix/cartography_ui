import React, { useEffect, useState } from 'react'
import { Box, Tab } from '@mui/material'
import Icon from 'components/Icon/Icon'
import { TabList as TabListMUI, TabPanel, TabContext } from '@mui/lab'

import './TabList.scss'

export interface Tab {
  name: string
  children: React.ReactNode
}

export interface TabListProps {
  tabs: Tab[]
}

const TabList: React.FC<TabListProps> = ({ tabs }) => {
  const [value, setValue] = useState<Tab['name']>()

  useEffect(() => {
    if (tabs.length && !value) setValue(tabs[0].name)
  }, [tabs, value])

  const handleChange = (e: React.SyntheticEvent, newValue: string) => {
    e.preventDefault()
    setValue(newValue)
  }

  return (
    <Box sx={{ width: '100%', typography: 'body1' }} className="tabsContainer">
      {!tabs.length || !value ? (
        <div className="noTab">
          <Icon name="no_data" width={220} />
        </div>
      ) : (
        <TabContext value={value}>
          <>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
              <TabListMUI
                onChange={handleChange}
                aria-label="tab"
                className="tabsList"
                scrollButtons={true}
                allowScrollButtonsMobile
                variant="scrollable"
              >
                {tabs.map(({ name }) => (
                  <Tab key={name} label={name} value={name} />
                ))}
              </TabListMUI>
            </Box>
            {tabs.map(({ name, children }) => (
              <TabPanel key={name} value={name}>
                {children}
              </TabPanel>
            ))}
          </>
        </TabContext>
      )}
    </Box>
  )
}

export default TabList
