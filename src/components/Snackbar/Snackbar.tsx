import React, { useEffect } from 'react'
import { Alert } from '@mui/material'
import SnackbarMUI from '@mui/material/Snackbar'

export interface SnackbarMessage {
  message: string
  key: number
}

export interface State {
  open: boolean
  snackPack: readonly SnackbarMessage[]
  messageInfo?: SnackbarMessage
}

interface SnackbarProps {
  isOpen?: boolean
  message?: string
  status?: 'error' | 'warning' | 'info' | 'success'
}

const Snackbar: React.FC<SnackbarProps> = ({
  isOpen = false,
  message = "Une erreur s'est produite",
  status = 'error'
}) => {
  const [open, setOpen] = React.useState(isOpen)

  useEffect(() => {
    if (isOpen) setOpen(true)
  }, [isOpen])

  const handleClose = (_event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return
    }

    setOpen(false)
  }

  return (
    <SnackbarMUI
      open={open}
      autoHideDuration={3000}
      onClose={handleClose}
      action={
        <Alert onClose={handleClose} severity={status} sx={{ width: '100%' }}>
          {message}
        </Alert>
      }
    />
  )
}

export default Snackbar
