import React, { useContext, type ComponentProps } from 'react'
import classNames from 'classnames'
import { ChosenTheme } from 'providers'
import CloseIcon from '@mui/icons-material/Close'
import { Box, Modal as ModalMUI } from '@mui/material'

import './Modal.scss'

export interface ModalProps extends ComponentProps<typeof ModalMUI> {
  onClose?: () => void
}

const Modal: React.FC<ModalProps> = ({ className, children, open = false, onClose, ...props }) => {
  const { theme } = useContext(ChosenTheme)

  const handleClose = (e: React.MouseEvent<SVGSVGElement, MouseEvent>) => {
    e.preventDefault()
    onClose?.()
  }

  return (
    <ModalMUI
      {...props}
      className={classNames(`modal-${theme} modal-wrapper`, className)}
      open={open}
      onClose={handleClose}
    >
      <Box className="modal-content" color="primary">
        <CloseIcon onClick={handleClose} className="close-icon" />
        {children}
      </Box>
    </ModalMUI>
  )
}

export default Modal
