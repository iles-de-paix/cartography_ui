// GLOBAL CSS
import 'react-datepicker/dist/react-datepicker.css'
import 'leaflet/dist/leaflet.css'
import './index.scss'

import Routes from './routes'
import { UserProvider } from './providers/UserProvider'
import { ReactQueryDevtools } from 'react-query/devtools'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ChosenThemeProvider, ThemeProvider } from './providers'

import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import { registerLocale, setDefaultLocale } from 'react-datepicker'

// DATE-FNS
import fr from 'date-fns/locale/fr'
setDefaultLocale('fr')
registerLocale('fr', fr)

import './i18n'

// Create a client
const queryClient = new QueryClient()

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)
root.render(
  <React.StrictMode>
    <ChosenThemeProvider>
      <ThemeProvider>
        <QueryClientProvider client={queryClient}>
          <UserProvider>
            <BrowserRouter>
              <Routes />
            </BrowserRouter>
          </UserProvider>
          <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
      </ThemeProvider>
    </ChosenThemeProvider>
  </React.StrictMode>
)
