import React, { createContext, useCallback, useEffect, useState } from 'react'
import type { PropsWithChildren } from 'react'

import type { User } from 'models/user'

export const localStorageKey = 'login-ile-de-paix'

interface IUserContext {
  user?: User
  setUser: (user?: User) => void
  setDemoMode: (isDemoMode: boolean) => void
}

export const UserContext = createContext<IUserContext>({} as IUserContext)

export const UserProvider: React.FC<PropsWithChildren> = ({ children }) => {
  const [user, setUserState] = useState<User>()
  // TODO: Demo mode -> to remove for prod perspective
  const [demoMode, setDemoMode] = useState(true)

  const setUser = useCallback(
    (_user?: User) => {
      if (_user) {
        localStorage.setItem(localStorageKey, _user.email)
        setUserState(_user)
      } else {
        // TODO: Demo mode -> to remove for prod perspective
        if (demoMode) {
          localStorage.setItem(localStorageKey, 'demo')
          setUserState({ email: 'demo@demo.com', id: 'demo', role: 1 })
        } else {
          // Keep this
          localStorage.removeItem(localStorageKey)
          setUserState(undefined)
        }
      }
    },
    [demoMode]
  )

  useEffect(() => {
    if (demoMode) setUser()
  }, [demoMode, setUser])

  return <UserContext.Provider value={{ user, setUser, setDemoMode }}>{children}</UserContext.Provider>
}
