import format from 'date-fns/format'
import * as Locales from 'date-fns/locale'
import _capitalize from 'lodash/capitalize'

export const getCompleteDate = (date: string) =>
  _capitalize(format(new Date(date), 'EEEE d MMM yyyy', { locale: Locales['fr'] }))
