import axios from 'axios'
import { t } from 'i18next'
import { ERole, type Role } from 'models/user'
import type { CampaignDate } from 'models/campaign'
import type { StreetMapLocation } from 'models/location'

export const getFormattedCurrency = (
  amount: number | undefined,
  numberFormat: string = 'fr-FR',
  currency: string = 'EUR'
) => {
  return new Intl.NumberFormat(numberFormat, { style: 'currency', currency }).format(amount || 0)
}

export const getRangeHours = (morningSelected: boolean | undefined, afternoonSelected: boolean | undefined) => {
  return (
    [
      { value: morningSelected, label: 'Matin' },
      { value: afternoonSelected, label: 'Après-midi' }
    ]
      .filter(i => !!i.value)
      .map(i => i.label)
      .join(' & ') || '?'
  )
}

export const getRoleName = (role?: number): string => t(`role.${ERole[role || 0] as Role}`)

// INFO & documentation for OPENSTREETMAP here: https://nominatim.org/release-docs/latest/api/Search/
export const searchLocation = async (
  searchValue: string,
  query: 'amenity' | 'street' | 'city' | 'county' | 'state' | 'country' | 'postalcode' | 'q' = 'q', // Cannot be combined with the q=<query> parameter.
  limit: number = 15,
  format: 'xml' | 'json' | 'jsonv2' | 'geojson' | 'geocodejson' = 'jsonv2'
): Promise<StreetMapLocation[]> => {
  const urlBase = 'https://nominatim.openstreetmap.org/search?'
  const url = `${urlBase}addressdetails=1&${query}=${searchValue}&format=${format}&limit=${limit}`
  return await axios
    .get<StreetMapLocation[]>(url)
    .then(response => response.data)
    .catch(error => {
      console.error(error)
      return []
    })
}

export const getGroupedCampaignDatesByCreatedDate = (campaignDates?: CampaignDate[]) => {
  if (!campaignDates) return {}

  return campaignDates.reduce<Record<CampaignDate['createdAt'], CampaignDate[]>>((acc, curr) => {
    if (!acc[curr.createdAt]) acc[curr.createdAt] = [curr]
    else acc[curr.createdAt].push(curr)
    return acc
  }, {})
}
