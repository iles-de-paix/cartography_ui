import axios, { type AxiosResponse } from 'axios'

const API_URL = process.env.REACT_APP_API

export const succeededStatus = [200, 201, 202, 204]

const fetchData = async <T>(key: string): Promise<AxiosResponse<T[]>> => {
  return await axios.get(`${API_URL}${key}`)
}

const fetchItem = async <T extends { id: string }>(key: string, id: T['id']): Promise<AxiosResponse<T[]>> => {
  return await axios.get(`${API_URL}${key}/${id}`)
}

const fetchFilteredData = async <T>(key: string, filters?: Record<string, string>): Promise<AxiosResponse<T[]>> => {
  if (!filters) return await axios.get(`${API_URL}${key}`)

  const queryString = Object.keys(filters)
    .map(_key => `${_key}=${filters[_key]}`)
    .join('&')

  return await axios.get(`${API_URL}${key}?${queryString}`)
}

const createData = async <T>(key: string, newData: T): Promise<AxiosResponse<T>> => {
  return await axios.post(`${API_URL}${key}`, newData, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

const updateData = async <T extends { id: string }>(key: string, updatedData: T): Promise<AxiosResponse<T>> => {
  // JSON Server format
  return await axios.put(`${API_URL}${key}/${updatedData.id}`, updatedData, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
  // return await axios.put(`${API_URL}${key}`, updatedData)
}

const deleteData = async <T extends { id: string }>(key: string, id: T['id']): Promise<AxiosResponse<T>> => {
  // JSON Server format
  return await axios.delete(`${API_URL}${key}/${id}`, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
  // return await axios.delete(`${API_URL}${key}?id=${id}`)
}

export { fetchData, fetchItem, fetchFilteredData, createData, updateData, deleteData }
