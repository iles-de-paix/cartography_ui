# cartography_ui

Repository: [GitLab](https://gitlab.com/iles-de-paix/cartography_ui)

## Getting started

Clone the repository on your computer

```
git clone https://gitlab.com/iles-de-paix/cartography_ui.git
```

Enter the folder

```
cd .\cartography_ui\
```

Install all packages

```
npm i
```

## Run the project

Start project

```
npm start
```

Run local server (implemented with JSON Server and stored in the `db.json` file)

```
npm run server
```

## Preview

![image](src/assets/ile-de-paix-1.png)

![image](src/assets/ile-de-paix-2.png)

![image](src/assets/ile-de-paix-3.png)

![image](src/assets/ile-de-paix-4.png)

## Deployment

CI/CD in [VERCEL](https://vercel.com/iles-de-pais/cartography-ui)

## Cartography UI

See [CARTOGRAPHY UI](https://cartography-ui.vercel.app/) deployed in VERCEL: **https://cartography-ui.vercel.app/**

Source code in [GITLAB](https://gitlab.com/iles-de-paix/cartography_ui): **https://gitlab.com/iles-de-paix/cartography_ui**

## Cartography API (JSON Server)

2 ways to work with JSON Server:

### 1) locally:

Update the url in the .env.development file:

```js
REACT_APP_API=http://localhost:8000
```

Launch the server (to read the db.json file)

```
npm run server
```

URL: _http://localhost:8000_

### 2) online:

The .env.development file should have this url:

```js
REACT_APP_API=https://cartography-api-json-server.vercel.app/
```

URL: *https://cartography-api-json-server.vercel.app/*

The deployed dedicated JSON Server in VERCEL

See [CARTOGRAPHY API](https://cartography-api-json-server.vercel.app/) deployed in VERCEL: **https://cartography-api-json-server.vercel.app/**

Source code in [GITLAB](https://gitlab.com/iles-de-paix/cartography_api_json_server): **https://gitlab.com/iles-de-paix/cartography_api_json_server**

## Structure

- assets: pictures (PNG), static locations (JSON) & icons (SVG)
- components: only generic components
- features: component with contexts and dependencies
- header: banner shared in all pages
- hooks: shared states & logic
- models: all models related to the data and API
- pages: all existing pages
- providers: theme and current user only
- services: generic and reusable CRUD (Create Read Update Delete) with axios
- styles: colors with themes shared on the entire project and apply on Material UI

## Assets, images, icons, maps and components lib

- [**React-leaflet**](https://react-leaflet.js.org/)
- [**GeoJson (Maps)**](https://www.odwb.be/explore/dataset/communesgemeente-belgium/information/)
- [**OpenStreetMap API**](https://nominatim.org/release-docs/latest/api/Search/)
- [**UNDRAW.CO**](https://undraw.co/)
- [**Material UI**](https://mui.com/)
- [**Material Icons**](https://mui.com/material-ui/material-icons/)

## Authors and acknowledgment

[Loïc Burnotte](https://gitlab.com/loic.burnotte)

## License

Open source project.
